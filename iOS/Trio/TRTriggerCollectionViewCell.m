//
//  TRTriggerCollectionViewCell.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRTriggerCollectionViewCell.h"

@implementation TRTriggerCollectionViewCell

- (void)awakeFromNib
{
    self.containerView.layer.cornerRadius = 5.0f;
    self.containerView.layer.masksToBounds = YES;
}

@end
