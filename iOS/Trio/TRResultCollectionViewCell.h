//
//  TRResultCollectionViewCell.h
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRResultCollectionViewCell : UICollectionViewCell

@property (nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutlet UIImageView *resultImageView;
@property (nonatomic) IBOutlet UIView *greenBackgroundView;

@end
