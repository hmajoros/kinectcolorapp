//
//  TRResultCollectionViewCell.m
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRResultCollectionViewCell.h"

@implementation TRResultCollectionViewCell

- (void)awakeFromNib
{
    self.layer.cornerRadius = 5.0f;
    self.layer.masksToBounds = YES;
}

@end
