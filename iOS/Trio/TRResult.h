//
//  TRResult.h
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>
#import "TRResultType.h"

@interface TRResult : NSObject

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot andResultType:(TRResultTypeIndex)index;
- (instancetype)initWithType:(NSInteger)result_id resourceCategory:(NSString *)resourceCategory andResourceFileName:(NSString *)resourceFileName;


@property (nonatomic) NSString *key;
@property (nonatomic) NSString *displayName;
@property (nonatomic) TRResultTypeIndex index;
@property (nonatomic) NSInteger result_id;

@property (nonatomic) NSString *resourceCategory;
@property (nonatomic) NSString *resourceFileName;

@end
