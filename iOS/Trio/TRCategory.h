//
//  TRCategory.h
//  Trio
//
//  Created by J. Christian Bator on 12/10/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

@interface TRCategory : NSObject

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot;

@property (nonatomic) NSString *key;
@property (nonatomic) NSString *displayName;

@end
