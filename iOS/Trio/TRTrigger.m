//
//  TRTrigger.m
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRTrigger.h"

@implementation TRTrigger

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot
{
    self = [super init];
    if (self)
    {
        self.displayName = [snapshot.key stringByReplacingOccurrencesOfString:@"-" withString:@" "];
        self.type = [snapshot.value integerValue];
    }
    return self;
}

- (instancetype)initWithType:(TRTriggerType)type
{
    self = [super init];
    if (self)
    {
        self.displayName = [self displayNames][@(type)];
        self.type = type;
    }
    return self;
}

- (NSDictionary *)displayNames
{
    return @{
             @(1) : @"First-touch",
             @(2) : @"Long-press",
             @(3) : @"Drag"
             };
}

@end
