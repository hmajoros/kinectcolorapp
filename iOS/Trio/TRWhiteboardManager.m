//
//  TRWhiteboardManager.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRWhiteboardManager.h"
#import "TRTheme.h"
#import "Firebase.h"
#import "TRGraphicView.h"
#import "TRSortedMutableArray.h"

@interface TRWhiteboardManager()
{
    NSComparator compare_graphics;
}

@property (nonatomic) NSMutableArray *sessions;

@property (nonatomic) NSInteger versionNum;

@end

@implementation TRWhiteboardManager

# pragma mark - Initializers
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^
      {
          sharedInstance = [[self alloc] init];
      });
    return sharedInstance;
}

- (void)initWhiteboard
{
    self.sessions = [NSMutableArray array];

    Firebase *user_ref = [[Firebase alloc] initWithUrl:@"https://triotriotrio.firebaseio.com/users/user_1"];
    [user_ref observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
    {
        [self.sessions removeAllObjects];
        for (FDataSnapshot *child in snapshot.children)
        {
            TRSession *session = [[TRSession alloc] initWithSnapshot:child];
            [self.sessions addObject:session];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sessions_loaded" object:nil];
    }];
    
    compare_graphics = ^(id obj1, id obj2)
    {
        if (((TRGraphic *)(obj2)).identifier > ((TRGraphic *)(obj1)).identifier)
        {
            return NSOrderedAscending;
        }
        else if (((TRGraphic *)(obj2)).identifier < ((TRGraphic *)(obj1)).identifier)
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedAscending;
        }
    };
}

# pragma mark - User session handling
- (NSInteger)getSessionCount
{
    return self.sessions.count;
}

- (TRSession *)getSessionAtIndex:(NSInteger)index
{
    return [self.sessions objectAtIndex:index];
}

- (void)saveSessionWithEnvironment:(UIView *)environment andTitle:(NSString *)title
{
    Firebase *user_ref = [[Firebase alloc] initWithUrl:@"https://triotriotrio.firebaseio.com/users/user_1"];
    
    Firebase *session_ref;
    if (self.session)
    {
        session_ref = [user_ref childByAppendingPath:self.session.sessionID];
    }
    else
    {
        session_ref = [user_ref childByAutoId];
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    NSString *dataPath = [docsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", session_ref.key]];
    
    UIGraphicsBeginImageContextWithOptions(environment.bounds.size, environment.opaque, 0.0);
    [environment.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *environment_image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *image_data = UIImagePNGRepresentation(environment_image);
    [image_data writeToFile:dataPath atomically:YES];
    
    NSMutableDictionary *session_dict = [NSMutableDictionary dictionary];
    [session_dict setObject:[self packageEnvironment:environment] forKey:@"environment"];
    [session_dict setObject:title forKey:@"title"];
    [session_ref setValue:session_dict];
}

# pragma mark - Prepare Whiteboard
- (void)resetWhiteboard
{
    self.versionNum = 0;
    self.pin = NSIntegerMin;
    self.background_color = [TRTheme canvasColor];
    self.background_category_key = @"none";
    
    self.background_music_category = nil;
    self.background_music_filename = nil;
    
    self.graphics = [NSMapTable mapTableWithKeyOptions:NSPointerFunctionsStrongMemory valueOptions:NSPointerFunctionsStrongMemory];
    self.selectedGraphicIdentifiers = [NSMutableArray array];
    self.session = nil;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchMode:) name:@"switch_mode" object:nil];
}

- (void)prepareWhiteboardForNewEnvironment
{
    [self resetWhiteboard];
}

- (void)prepareWhiteboardFromExistingEnvironmentAtIndex:(NSInteger)index
{
    [self resetWhiteboard];
    self.session = [self.sessions objectAtIndex:index];
    [self unpackageEnvironment:self.session.environment];
}

# pragma mark - Action mode
- (void)switchMode:(NSNotification *)notification
{
    NSDictionary *user_info = notification.userInfo;
    BOOL actionMode = [[user_info objectForKey:@"action_mode"] boolValue];
    self.actionMode = actionMode;
    
    if (actionMode)
    {
        NSInteger graphicID = [[user_info objectForKey:@"graphic_id"] integerValue];
        self.selectedGraphicID = graphicID;
    }
}

# pragma mark - Background Color and Images
- (void)updateBackgroundColor:(UIColor *)color
{
    self.background_image_name = nil;
    self.background_category_key = @"none";
    self.background_color = color;
    [_delegate backgroundColorUpdatedWithColor:color];
}

- (void)updateBackgroundImageWithName:(NSString *)image_name andCategory:(NSString *)category_key andImage:(UIImage *)image
{
    self.background_color = nil;
    self.background_image_name = image_name;
    self.background_category_key = category_key;
    [_delegate backgroundImageUpdatedWithImage:image];
}

# pragma mark - Graphics
- (TRGraphic *)getGraphicWithIdentifier:(NSInteger)identifier
{
    return [self.graphics objectForKey:@(identifier)];
}

- (void)addGraphic:(TRGraphic *)graphic
{
    [self.graphics setObject:graphic forKey:@(graphic.identifier)];
    [_delegate graphicAdded:graphic];
}

- (void)deleteSelectedGraphics
{
    for (NSNumber *graphicIdentifier in self.selectedGraphicIdentifiers)
    {
        TRGraphic *victim = [self.graphics objectForKey:graphicIdentifier];
        if (victim)
        {
            [_delegate graphicRemoved:victim];
            [self.graphics removeObjectForKey:@(victim.identifier)];
        }
    }
    [self.selectedGraphicIdentifiers removeAllObjects];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"delete_status" object:nil userInfo:@{@"canDelete" : @(NO)}];
}

# pragma mark - Events
- (void)addTrigger:(TRTrigger *)trigger toGraphicWithIdentifier:(NSInteger)graphicIdentifier
{
    TRGraphic *graphic = [self.graphics objectForKey:@(graphicIdentifier)];
    
    if (graphic)
    {
        [graphic.actions addObject:trigger];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"events_updated" object:nil];
    }
}

- (void)addResult:(TRResult *)result toGraphicWithIdentifier:(NSInteger)graphicIdentifier
{
    TRGraphic *graphic = [self.graphics objectForKey:@(graphicIdentifier)];
    
    if (graphic)
    {
        [graphic.actions addObject:result];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"events_updated" object:nil];
    }
}

- (void)removeEventAtIndex:(NSInteger)index fromGraphicWithIdentifier:(NSInteger)graphicIdentifier
{
    TRGraphic *graphic = [self.graphics objectForKey:@(graphicIdentifier)];
    
    if (graphic)
    {
        [graphic.actions removeObjectAtIndex:index];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"events_updated" object:nil];
    }
}

# pragma mark - Packaging
- (NSDictionary *)packageEnvironment:(UIView *)environment
{
    NSMutableDictionary *environment_dict = [NSMutableDictionary dictionary];
    if (self.background_color)
    {
        [environment_dict setObject:[TRTheme hexStringFromColor:self.background_color] forKey:@"background_color"];
    }
    else
    {
        [environment_dict setObject:self.background_image_name forKey:@"background_image_filename"];
        [environment_dict setObject:self.background_category_key forKey:@"background_image_category"];
        [environment_dict setObject:@"backgrounds" forKey:@"background_image_type"];
    }
    
    if (self.background_music_filename)
    {
        [environment_dict setObject:self.background_music_filename forKey:@"background_music_filename"];
        [environment_dict setObject:self.background_music_category forKey:@"background_music_category"];
        [environment_dict setObject:@"music" forKey:@"background_music_type"];
    }
    
    TRSortedMutableArray *graphics_list = [[TRSortedMutableArray alloc] initWithComparator:compare_graphics];
    for (TRGraphic *graphic in self.graphics.objectEnumerator)
    {
        for (UIView *view in environment.subviews)
        {
            if ([view isKindOfClass:[TRGraphicView class]])
            {
                TRGraphicView *graphicView = (TRGraphicView *)view;
                if (graphicView.identifier == graphic.identifier)
                {
                    graphic.width_ratio = graphicView.frame.size.width / environment.frame.size.width;
                    graphic.height_ratio = graphicView.frame.size.height / environment.frame.size.height;
                    
                    graphic.x_ratio = 1.0 - ((graphicView.frame.origin.x + graphicView.frame.size.width) / environment.frame.size.width);
                    graphic.y_ratio = graphicView.frame.origin.y / environment.frame.size.height;
                    
                    break;
                }
            }
        }
        
        [graphics_list addObject:graphic];
    }
    
    NSMutableArray *graphic_data = [NSMutableArray array];
    for (TRGraphic *graphic in graphics_list)
    {
        [graphic_data addObject:graphic.toJSON];
    }
    [environment_dict setObject:graphic_data forKey:@"graphics"];
    
    return environment_dict;
}

# pragma mark - Unpackaging
- (void)unpackageEnvironment:(NSDictionary *)environment
{
    NSString *hex_background_color = environment[@"background_color"];
    
    if (!hex_background_color)
    {
        self.background_image_name = environment[@"background_image_filename"];
        self.background_category_key = environment[@"background_image_category"];
    }
    else
    {
        self.background_color = SKColorFromHexString(hex_background_color);
    }
    
    NSArray *graphics = environment[@"graphics"];
    for (NSDictionary *graphic_dict in graphics)
    {
        TRGraphic *graphic = [[TRGraphic alloc] initWithJSON:graphic_dict];
        [self.graphics setObject:graphic forKey:@(graphic.identifier)];
    }
}

void SKScanHexColor(NSString * hexString, float * red, float * green, float * blue, float * alpha) {
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    if (red) { *red = ((baseValue >> 24) & 0xFF)/255.0f; }
    if (green) { *green = ((baseValue >> 16) & 0xFF)/255.0f; }
    if (blue) { *blue = ((baseValue >> 8) & 0xFF)/255.0f; }
    if (alpha) { *alpha = ((baseValue >> 0) & 0xFF)/255.0f; }
}

UIColor * SKColorFromHexString(NSString *hexString)
{
    float red, green, blue, alpha;
    SKScanHexColor(hexString, &red, &green, &blue, &alpha);
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

# pragma mark - Sending
- (BOOL)sendEnvironment:(UIView *)environment
{
     if (self.pin > 0)
     {
         Firebase *ref = [[Firebase alloc] initWithUrl:@"https://triotriotrio.firebaseio.com"];
         Firebase *environment_ref = [ref childByAppendingPath:[NSString stringWithFormat:@"environments/%d", self.pin]];
         [environment_ref setValue:[self packageEnvironment:environment]];
         
         Firebase *pin_ref = [[Firebase alloc] initWithUrl:@"https://triotriotrio.firebaseio.com"];
         pin_ref = [pin_ref childByAppendingPath:[NSString stringWithFormat:@"sessions/%d", self.pin]];
         [pin_ref setValue:@(self.versionNum++)];
         
         return YES;
     }
     
     return NO;
}

# pragma mark - Removing
- (void)removeEnvironment
{
    Firebase *ref = [[Firebase alloc] initWithUrl:@"https://triotriotrio.firebaseio.com"];
    Firebase *environment_ref = [ref childByAppendingPath:[NSString stringWithFormat:@"environments/%d", self.pin]];
    Firebase *session_ref = [ref childByAppendingPath:[NSString stringWithFormat:@"sessions/%d", self.pin]];
    
    [environment_ref removeValue];
    [session_ref removeValue];
    self.pin = NSIntegerMin;
}

# pragma mark - Cleanup
- (void)cleanupWhiteboard
{
    self.session = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
