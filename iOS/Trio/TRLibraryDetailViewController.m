//
//  TRLibraryDetailViewController.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRLibraryDetailViewController.h"
#import "TRDetailTableViewCell.h"
#import "TRTheme.h"
#import "TRCategory.h"

#import "TRLibrarySelectionViewController.h"

#define kTRDetailTableViewCellIdentifier @"DetailCell"

@interface TRLibraryDetailViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSArray *dataSource;
@property (nonatomic) NSInteger selectedIndex;

@property (nonatomic) IBOutlet UITableView *detailTableView;

@end

@implementation TRLibraryDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [TRTheme backgroundColor];
    self.detailTableView.backgroundColor = [TRTheme backgroundColor];
    self.detailTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style: UIBarButtonItemStylePlain target:nil action:nil];
    
    switch (self.detailType)
    {
        case TRLibraryDetailTypeBackgrounds:
            self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Backgrounds"];
            break;
        case TRLibraryDetailTypeGraphics:
            self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Graphics"];
            break;
        case TRLibraryDetailTypeResults:
            self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Results"];
            break;
        case TRLibraryDetailTypeMusic:
            self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Music"];
            break;
        default:
            break;
    }
    
    switch (self.detailType)
    {
        case TRLibraryDetailTypeBackgrounds:
            [self loadBackgroundsData];
            break;
        case TRLibraryDetailTypeGraphics:
            [self loadGraphicsData];
            break;
        case TRLibraryDetailTypeResults:
            [self loadResultsData];
            break;
        case TRLibraryDetailTypeMusic:
            [self loadMusicData];
            break;
        default:
            break;
    }
}

# pragma mark - Data Sources
- (void)loadBackgroundsData
{
    [[TRLibraryManager sharedInstance] getBackgroundCategoriesWithBlock:^(NSArray *backgroundCategories)
    {
        self.dataSource = backgroundCategories;
        [self.detailTableView reloadData];
    }];
}

- (void)loadGraphicsData
{
    [[TRLibraryManager sharedInstance] getGraphicCategoriesWithBlock:^(NSArray *graphicCategories)
     {
         self.dataSource = graphicCategories;
         [self.detailTableView reloadData];
     }];
}

- (void)loadResultsData
{
    [[TRLibraryManager sharedInstance] getResultTypesWithBlock:^(NSArray *resultTypes)
     {
         self.dataSource = resultTypes;
         [self.detailTableView reloadData];
     }];
}

- (void)loadMusicData
{
    [[TRLibraryManager sharedInstance] getMusicCategoriesWithBlock:^(NSArray *musicCategories)
     {
         self.dataSource = musicCategories;
         [self.detailTableView reloadData];
     }];
}

# pragma mark - Table View
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TRDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRDetailTableViewCellIdentifier];
    
    id object = [self.dataSource objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[TRResultType class]])
    {
        TRResultType *type = (TRResultType *)object;
        cell.titleLabel.text = type.displayName;
    }
    else
    {
        TRCategory *category = (TRCategory *)object;
        cell.titleLabel.text = category.displayName;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"detailTOselection" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    TRLibrarySelectionViewController *destination = segue.destinationViewController;
    destination.detailType = self.detailType;
    
    id object = [self.dataSource objectAtIndex:self.selectedIndex];
    if ([object isKindOfClass:[TRResultType class]])
    {
        TRResultType *type = (TRResultType *)object;
        destination.type = type;
    }
    else
    {
        TRCategory *category = (TRCategory *)object;
        destination.category = category;
    }
}

@end
