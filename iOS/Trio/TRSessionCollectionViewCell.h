//
//  TRSessionCollectionViewCell.h
//  Trio
//
//  Created by J. Christian Bator on 12/18/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRSessionCollectionViewCell : UICollectionViewCell

@property (nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutlet UIImageView *sessionImageView;

@end
