//
//  TRTitleViewController.m
//  Trio
//
//  Created by J. Christian Bator on 12/18/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRTitleViewController.h"
#import "TRTheme.h"
#import "POP.h"

@interface TRTitleViewController () <UITextFieldDelegate>

@property (nonatomic) IBOutlet NSLayoutConstraint *topSpace;

@property (nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (nonatomic) IBOutlet UIView *container;
@property (nonatomic) IBOutlet UITextField *inputField;

@end

@implementation TRTitleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.inputField.tintColor = [TRTheme accentColor];
    
    self.container.layer.cornerRadius = 5.0f;
    self.container.layer.masksToBounds = YES;
    
    self.inputField.delegate = self;
    self.inputField.returnKeyType = UIReturnKeyDone;
}

# pragma mark - Text field delegate
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
    [theTextField resignFirstResponder];
    [self savePressed:nil];
    
    return NO;
}

# pragma mark - IBActions
- (IBAction)savePressed:(id)sender
{
    [self.delegate userSelectedTitle:self.inputField.text];
    [self removeFromView];
}

- (IBAction)cancelPressed:(id)sender
{
    [self removeFromView];
}

# pragma mark - Animating entrance and exit
- (void)showInView:(UIView *)parent_view
{
    [parent_view addSubview:self.view];
    
    self.blurView.alpha = 0;
    self.topSpace.constant = -250;
    
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayoutConstraintConstant];
    positionAnimation.toValue = @(120);
    
    positionAnimation.springBounciness = 10;
    positionAnimation.removedOnCompletion = YES;
    
    __weak TRTitleViewController *weak_self = self;
    [UIView animateWithDuration:0.3 animations:^
     {
         weak_self.blurView.alpha = 0.9;
     }];
    
    [positionAnimation setCompletionBlock:^(POPAnimation *animation, BOOL complete)
     {
         [weak_self.inputField becomeFirstResponder];
     }];
    
    [self.topSpace pop_addAnimation:positionAnimation forKey:@"position"];
}

- (void)removeFromView
{
    __weak TRTitleViewController *weak_self = self;
    
    // Position
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayoutConstraintConstant];
    positionAnimation.toValue = @(-250);
    positionAnimation.springBounciness = 10;
    positionAnimation.removedOnCompletion = YES;
    [self.topSpace pop_addAnimation:positionAnimation forKey:@"position"];
    
    // Blur opacity
    POPBasicAnimation *blur_animation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
    blur_animation.toValue = @(0);
    blur_animation.duration = 0.3;
    blur_animation.removedOnCompletion = YES;
    [blur_animation setCompletionBlock:^(POPAnimation *animation, BOOL finished){
        [weak_self.view removeFromSuperview];
        [weak_self.delegate viewRemoved];
    }];
    
    [self.blurView pop_addAnimation:blur_animation forKey:@"opacity"];
}


@end
