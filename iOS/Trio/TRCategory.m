//
//  TRCategory.m
//  Trio
//
//  Created by J. Christian Bator on 12/10/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRCategory.h"

@implementation TRCategory

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot
{
    self = [super init];
    if (self)
    {
        self.key = snapshot.key;
        self.displayName = [self.key stringByReplacingOccurrencesOfString:@"-" withString:@" "];
    }
    return self;
}

@end
