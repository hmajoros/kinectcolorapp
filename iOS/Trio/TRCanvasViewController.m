//
//  TRCanvasViewController.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRCanvasViewController.h"
#import "FCColorPickerViewController.h"
#import "TRTheme.h"
#import "THPinViewController.h"
#import "TRWhiteboardManager.h"
#import "TRWhiteboardViewController.h"
#import "POP.h"
#import "TRTrigger.h"
#import "TRResult.h"
#import "TRTriggerCollectionViewCell.h"
#import "TRResultCollectionViewCell.h"

#define kTRTriggerCellIdentifier @"TriggerCell"
#define kTRResultCellIdentifier @"ResultCell"

@interface TRCanvasViewController () <FCColorPickerViewControllerDelegate, THPinViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic) IBOutlet UILabel *sessionRunningLabel;
@property (nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic) IBOutlet UIView *toolBarView;
@property (nonatomic) IBOutlet UIButton *deleteButton;
@property (nonatomic) IBOutlet UIButton *changeBackroundColorButton;
@property (nonatomic) IBOutlet UIButton *recipeButton;
@property (nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (nonatomic) UIViewController *whiteboardViewController;

@property (nonatomic) NSMutableArray *actionContent;
@property (nonatomic) NSInteger selectedEventIndex;
@property (nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) IBOutlet UIButton *deleteEventButton;
@property (nonatomic) IBOutlet UIButton *deselectEventButton;

@end

@implementation TRCanvasViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [TRTheme backgroundColor];
    self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Canvas"];
    
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"Stop Session" style:UIBarButtonItemStylePlain target:self action:@selector(resetPin:)];
    
    NSDictionary *normal_attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"AvenirNext-Medium" size:17.0],
                                NSFontAttributeName,
                                [UIColor whiteColor],
                                NSForegroundColorAttributeName, nil];
    [button setTitleTextAttributes:normal_attributes forState:UIControlStateNormal];
    
    NSDictionary *disabled_attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [UIFont fontWithName:@"AvenirNext-Medium" size:17.0],
                                       NSFontAttributeName,
                                       [TRTheme disabledColor],
                                       NSForegroundColorAttributeName, nil];
    [button setTitleTextAttributes:disabled_attributes forState:UIControlStateDisabled];
    
    self.navigationItem.leftBarButtonItem = button;
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    
    self.toolBarView.backgroundColor = [TRTheme backgroundColor];
    self.toolBarView.layer.cornerRadius = 5.0f;
    self.toolBarView.layer.borderWidth = 1.5f;
    self.toolBarView.layer.borderColor = [TRTheme canvasColor].CGColor;
    self.toolBarView.layer.masksToBounds = YES;
    
    self.deleteButton.tintColor = [TRTheme accentColor];
    self.deleteButton.enabled = NO;
    
    self.recipeButton.tintColor = [TRTheme accentColor];
    self.recipeButton.enabled = NO;
    
    self.blurView.layer.cornerRadius = 5.0f;
    self.blurView.layer.masksToBounds = YES;
    self.blurView.hidden = YES;
    
    self.changeBackroundColorButton.tintColor = [TRTheme accentColor];
    
    self.selectedEventIndex = NSIntegerMin;
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.deleteEventButton.tintColor = [UIColor redColor];
    self.deselectEventButton.tintColor = [UIColor whiteColor];
    
    self.deleteEventButton.hidden = YES;
    self.deselectEventButton.hidden = YES;
    
    [self.sessionRunningLabel setHidden:YES];
    [self.activityIndicatorView setColor:[TRTheme accentColor]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveDeleteStatusUpdate:) name:@"delete_status" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchMode:) name:@"switch_mode" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleEventsUpdated:) name:@"events_updated" object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    for (UIViewController *viewController in self.childViewControllers)
    {
        if ([viewController isKindOfClass:[TRWhiteboardViewController class]])
        {
            self.whiteboardViewController = viewController;
            break;
        }
    }
}

# pragma mark - Data notification handling
- (void)didReceiveDeleteStatusUpdate:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"delete_status"])
    {
        NSDictionary *userInfo = notification.userInfo;
        BOOL canDelete = [[userInfo objectForKey:@"canDelete"] boolValue];
        [self.deleteButton setEnabled:canDelete];
    }
}

- (void)switchMode:(NSNotification *)notification
{
    NSDictionary *user_info = notification.userInfo;
    BOOL actionMode = [[user_info objectForKey:@"action_mode"] boolValue];
    [self.recipeButton setEnabled:actionMode];
}

- (void)handleEventsUpdated:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"events_updated"])
    {
        self.actionContent = [[TRWhiteboardManager sharedInstance] getGraphicWithIdentifier:[TRWhiteboardManager sharedInstance].selectedGraphicID].actions;
        [self.collectionView reloadData];
    }
}


# pragma mark - Events Collection View
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.actionContent objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[TRTrigger class]])
    {
        return CGSizeMake(184, 140);
    }

    return CGSizeMake(160, 140);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.actionContent.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.actionContent objectAtIndex:indexPath.row];
    if ([object isKindOfClass:[TRTrigger class]])
    {
        TRTriggerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTRTriggerCellIdentifier forIndexPath:indexPath];
        TRTrigger *trigger = (TRTrigger *)object;
        cell.titleLabel.text = trigger.displayName;
        cell.triggerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"trigger_%ld", (long)trigger.type]];
        
        if (trigger.type == TRTriggerTypeDrag)
        {
            cell.connector.hidden = YES;
            cell.greenBackgroundView.layer.cornerRadius = 5.0f;
            cell.greenBackgroundView.layer.masksToBounds = YES;
        }
        else
        {
            cell.connector.hidden = NO;
            cell.greenBackgroundView.layer.cornerRadius = 0.0f;
            cell.greenBackgroundView.layer.masksToBounds = NO;
        }
        
        if (self.selectedEventIndex == indexPath.row)
        {
            cell.greenBackgroundView.layer.borderColor = [[UIColor redColor] CGColor];
            cell.greenBackgroundView.layer.borderWidth = 4.0;
        }
        else
        {
            cell.greenBackgroundView.layer.borderColor = nil;
            cell.greenBackgroundView.layer.borderWidth = 0.0;
        }
        
        cell.layer.cornerRadius = 5.0f;
        cell.layer.masksToBounds = YES;
        return cell;
    }
    else if ([object isKindOfClass:[TRResult class]])
    {
        TRResultCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTRResultCellIdentifier forIndexPath:indexPath];
        TRResult *result = (TRResult *)object;
        cell.titleLabel.text = result.displayName;
        
        switch (result.index)
        {
            case TRResultTypeIndexSounds:
            {
                cell.resultImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"result_%ld", (long)result.index]];
                break;
            }
            case TRResultTypeIndexUpdateGraphics:
            {
                cell.resultImageView.image = [UIImage imageNamed:result.key];
                break;
            }
            case TRResultTypeIndexBackgroundMusic:
            {
                cell.resultImageView.image = [UIImage imageNamed:result.key];
                break;
            }
            default:
                break;
        }

        if (self.selectedEventIndex == indexPath.row)
        {
            cell.greenBackgroundView.layer.borderColor = [[UIColor redColor] CGColor];
            cell.greenBackgroundView.layer.borderWidth = 4.0;
        }
        else
        {
            cell.greenBackgroundView.layer.borderColor = nil;
            cell.greenBackgroundView.layer.borderWidth = 0.0;
        }
        
        return cell;
    }
    
    return [UICollectionViewCell new];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.selectedEventIndex > NSIntegerMin)
    {
        [self deselectEvent:nil];
    }
    self.selectedEventIndex = indexPath.row;
    [self.collectionView reloadData];
    
    self.deleteEventButton.hidden = NO;
    self.deselectEventButton.hidden = NO;
}

- (IBAction)deselectEvent:(id)sender
{
    if (self.selectedEventIndex > NSIntegerMin)
    {
        self.selectedEventIndex = NSIntegerMin;
        [self.collectionView reloadData];
    }
    
    self.deleteEventButton.hidden = YES;
    self.deselectEventButton.hidden = YES;
}

# pragma mark - Session management
- (void)sendEnvironment
{
    if ([[TRWhiteboardManager sharedInstance] sendEnvironment:self.whiteboardViewController.view])
    {
        [self.sessionRunningLabel setText:[NSString stringWithFormat:@"Session running with PIN: %ld", (long)[TRWhiteboardManager sharedInstance].pin]];
        [self.sessionRunningLabel setHidden:NO];
        [self.activityIndicatorView startAnimating];
    }
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
}

# pragma mark - Pin handling
- (NSUInteger)pinLengthForPinViewController:(THPinViewController *)pinViewController
{
    return 4;
}

- (BOOL)pinViewController:(THPinViewController *)pinViewController isPinValid:(NSString *)pin
{
    [TRWhiteboardManager sharedInstance].pin = [pin integerValue];
    [self sendEnvironment];
    return YES;
}

- (BOOL)userCanRetryInPinViewController:(THPinViewController *)pinViewController
{
    return YES;
}

# pragma mark - Bar button items
- (IBAction)playPressed:(id)sender
{
    if ([TRWhiteboardManager sharedInstance].pin < 0)
    {
        THPinViewController *pinViewController = [[THPinViewController alloc] initWithDelegate:self];
        pinViewController.promptTitle = @"Enter PIN";
        pinViewController.promptColor = [UIColor darkTextColor];
        pinViewController.view.tintColor = [TRTheme accentColor];
        pinViewController.hideLetters = YES;
        
        self.view.tag = THPinViewControllerContentViewTag;
        pinViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        pinViewController.translucentBackground = NO;
        
        [self presentViewController:pinViewController animated:YES completion:nil];
    }
    else
    {
        [self sendEnvironment];
    }
}

- (IBAction)resetPin:(id)sender
{
    [[TRWhiteboardManager sharedInstance] removeEnvironment];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [self.sessionRunningLabel setHidden:YES];
    [self.activityIndicatorView stopAnimating];
}

# pragma mark - Deleting graphics
- (IBAction)deletePressed:(id)sender
{
    [[TRWhiteboardManager sharedInstance] deleteSelectedGraphics];
}

# pragma mark - Show and hide recipe
- (IBAction)handleRecipePressed:(id)sender
{
    if (self.blurView.hidden)
    {
        self.actionContent = [[TRWhiteboardManager sharedInstance] getGraphicWithIdentifier:[TRWhiteboardManager sharedInstance].selectedGraphicID].actions;
        self.selectedEventIndex = NSIntegerMin;
        [self.collectionView reloadData];
        
        [self.deleteButton setEnabled:NO];
        [self.recipeButton setTitle:@"Hide Events" forState:UIControlStateNormal];
        self.blurView.alpha = 0;
        [self.blurView setHidden:NO];
        
        POPBasicAnimation *opacity = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        opacity.fromValue = @(0.0);
        opacity.toValue = @(0.95);
        opacity.duration = 0.3;
        [self.blurView pop_addAnimation:opacity forKey:@"opacity"];
    }
    else
    {
        [self.deleteButton setEnabled:YES];
        [self.recipeButton setTitle:@"Show Events" forState:UIControlStateNormal];
        
        POPBasicAnimation *opacity = [POPBasicAnimation animationWithPropertyNamed:kPOPViewAlpha];
        opacity.fromValue = @(0.95);
        opacity.toValue = @(0.0);
        opacity.duration = 0.3;
        
        [opacity setCompletionBlock:^(POPAnimation *animation, BOOL finished)
        {
           [self.blurView setHidden:YES];
        }];
        [self.blurView pop_addAnimation:opacity forKey:@"opacity"];
    }
}

# pragma mark - Deleting events
- (IBAction)deleteEventPressed:(id)sender
{
    if (self.selectedEventIndex > NSIntegerMin)
    {
        NSInteger victim_index = self.selectedEventIndex;
        self.selectedEventIndex = NSIntegerMin;
        
        [[TRWhiteboardManager sharedInstance] removeEventAtIndex:victim_index fromGraphicWithIdentifier:[TRWhiteboardManager sharedInstance].selectedGraphicID];
    }
    
    self.deleteEventButton.hidden = YES;
    self.deselectEventButton.hidden = YES;
}

# pragma mark - Changing background color
- (IBAction)chooseColor:(id)sender
{
    FCColorPickerViewController *colorPicker = [FCColorPickerViewController colorPicker];
    colorPicker.tintColor = [UIColor whiteColor];
    colorPicker.backgroundColor = [TRTheme neutralColor];
    colorPicker.color = [self.childViewControllers firstObject].view.backgroundColor;
    colorPicker.delegate = self;
    
    [colorPicker setModalPresentationStyle:UIModalPresentationFormSheet];
    [self presentViewController:colorPicker animated:YES completion:nil];
}

- (void)colorPickerViewController:(FCColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color
{
    [[TRWhiteboardManager sharedInstance] updateBackgroundColor:color];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)colorPickerViewControllerDidCancel:(FCColorPickerViewController *)colorPicker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
