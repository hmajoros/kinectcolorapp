//
//  TRTriggerCollectionViewCell.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRTriggerCollectionViewCell : UICollectionViewCell

@property (nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic) IBOutlet UIImageView *triggerImageView;
@property (nonatomic) IBOutlet UIView *greenBackgroundView;
@property (nonatomic) IBOutlet UIView *containerView;
@property (nonatomic) IBOutlet UIView *connector;

@end
