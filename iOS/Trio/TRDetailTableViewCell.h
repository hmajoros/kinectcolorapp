//
//  TRDetailTableViewCell.h
//  Trio
//
//  Created by J. Christian Bator on 12/10/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRDetailTableViewCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel *titleLabel;

@end
