//
//  TRWhiteboardViewController.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRWhiteboardViewController.h"
#import "TRWhiteboardManager.h"
#import "TRGraphic.h"
#import "TRGraphicView.h"
#import <POP/POP.h>
#import "TRTheme.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageManager.h>

#define kTRAWSBaseUrl @"https://s3.amazonaws.com/triotriotrio"

@interface TRWhiteboardViewController () <TRWhiteboardDelegate>

@property (nonatomic) BOOL loadedExistingEnvironment;
@property (nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation TRWhiteboardViewController

# pragma mark - View setup
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [TRWhiteboardManager sharedInstance].delegate = self;
    
    self.view.layer.cornerRadius = 5.0f;
    self.view.layer.masksToBounds = YES;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(deselectAllGraphicViews)];
    [self.view addGestureRecognizer:tap];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if (!self.loadedExistingEnvironment)
    {
        if ([TRWhiteboardManager sharedInstance].session)
        {
            if ([TRWhiteboardManager sharedInstance].background_color)
            {
                [self backgroundColorUpdatedWithColor:[TRWhiteboardManager sharedInstance].background_color];
            }
            else
            {
                NSString *url_string = [NSString stringWithFormat:@"%@/backgrounds/%@/%@", kTRAWSBaseUrl, [TRWhiteboardManager sharedInstance].background_category_key, [TRWhiteboardManager sharedInstance].background_image_name];
                
                __weak TRWhiteboardViewController *weak_self = self;
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:[NSURL URLWithString:url_string] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
                 {
                     if (image)
                     {
                         weak_self.backgroundImageView.image = image;
                     }
                 }];
            }
            
            for (TRGraphic *graphic in [TRWhiteboardManager sharedInstance].graphics.objectEnumerator)
            {
                NSString *url_string = [NSString stringWithFormat:@"%@/graphics/%@/%@", kTRAWSBaseUrl, graphic.category, graphic.filename];
                
                __weak TRWhiteboardViewController *weak_self = self;
                SDWebImageManager *manager = [SDWebImageManager sharedManager];
                [manager downloadImageWithURL:[NSURL URLWithString:url_string] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
                 {
                     if (image)
                     {
                         graphic.image = image;
                         
                         TRGraphicView *graphicView = [[TRGraphicView alloc] initWithGraphic:graphic];
                         
                         CGFloat width = graphic.width_ratio * self.view.frame.size.width;
                         CGFloat height = graphic.height_ratio * self.view.frame.size.height;
                         CGFloat x_coord = ((graphic.x_ratio - 1.0) * -1) * self.view.frame.size.width - width;
                         CGFloat y_coord = graphic.y_ratio * self.view.frame.size.height;
                         
                         graphicView.frame = CGRectMake(x_coord, y_coord, width, height);
                         
                         UITapGestureRecognizer *tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
                         [graphicView addGestureRecognizer:tap_recognizer];
                         
                         UIPanGestureRecognizer *pan_recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
                         [graphicView addGestureRecognizer:pan_recognizer];
                         
                         [weak_self.view addSubview:graphicView];
                     }
                 }];
            }
        }
        else
        {
            self.view.backgroundColor = [TRTheme canvasColor];
            [[TRWhiteboardManager sharedInstance] updateBackgroundColor:[TRTheme canvasColor]];
        }

        self.loadedExistingEnvironment = YES;
    }
}

# pragma mark - TRWhiteboardManager Delegate
- (void)backgroundColorUpdatedWithColor:(UIColor *)color
{
    self.backgroundImageView.image = nil;
    self.view.backgroundColor = color;
}

- (void)backgroundImageUpdatedWithImage:(UIImage *)image
{
    self.backgroundImageView.image = image;
}

- (void)graphicAdded:(TRGraphic *)graphic
{
    TRGraphicView *graphicView = [[TRGraphicView alloc] initWithGraphic:graphic];
    graphicView.center = self.view.center;
    
    UITapGestureRecognizer *tap_recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [graphicView addGestureRecognizer:tap_recognizer];
    
    UIPanGestureRecognizer *pan_recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [graphicView addGestureRecognizer:pan_recognizer];
    
    [self.view addSubview:graphicView];
}

- (void)graphicRemoved:(TRGraphic *)graphic
{
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[TRGraphicView class]])
        {
            TRGraphicView *graphicView = (TRGraphicView *)view;
            if (graphicView.identifier == graphic.identifier)
            {
                [graphicView removeFromSuperview];
                break;
            }
        }
    }
}

# pragma mark - Selection
- (void)handleTap:(UITapGestureRecognizer *)sender
{
    TRGraphicView *graphicView = (TRGraphicView *)sender.view;
    if (graphicView.borderView.hidden)
    {
        [self selectGraphicView:graphicView];
    }
    else
    {
        [self deselectGraphicView:graphicView];
    }
}

- (void)selectGraphicView:(TRGraphicView *)graphicView
{
    [graphicView showEditingHandles];
    for (UIGestureRecognizer *gestureRecognizer in graphicView.gestureRecognizers)
    {
        if (![gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]])
        {
            [graphicView removeGestureRecognizer:gestureRecognizer];
        }
    }
    
    [[TRWhiteboardManager sharedInstance].selectedGraphicIdentifiers addObject:@(graphicView.identifier)];
    
    // Notify others about state
    if ([TRWhiteboardManager sharedInstance].selectedGraphicIdentifiers.count == 1)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"switch_mode" object:nil userInfo:@{@"action_mode" : @(YES), @"graphic_id" : @(graphicView.identifier)}];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"switch_mode" object:nil userInfo:@{@"action_mode" : @(NO)}];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"delete_status" object:nil userInfo:@{@"canDelete" : @(YES)}];
}

- (void)deselectGraphicView:(TRGraphicView *)graphicView
{
    [graphicView hideEditingHandles];
    
    UIPanGestureRecognizer *pan_recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [graphicView addGestureRecognizer:pan_recognizer];
    
    [[TRWhiteboardManager sharedInstance].selectedGraphicIdentifiers removeObject:@(graphicView.identifier)];
    
    // Notify others about state
    if ([TRWhiteboardManager sharedInstance].selectedGraphicIdentifiers.count > 0)
    {
        if ([TRWhiteboardManager sharedInstance].selectedGraphicIdentifiers.count == 1)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"switch_mode" object:nil userInfo:@{@"action_mode" : @(YES), @"graphic_id" : @(graphicView.identifier)}];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"switch_mode" object:nil userInfo:@{@"action_mode" : @(NO)}];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"delete_status" object:nil userInfo:@{@"canDelete" : @(YES)}];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"delete_status" object:nil userInfo:@{@"canDelete" : @(NO)}];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"switch_mode" object:nil userInfo:@{@"action_mode" : @(NO)}];
    }
}

- (void)deselectAllGraphicViews
{
    for (UIView *view in self.view.subviews)
    {
        if ([view isKindOfClass:[TRGraphicView class]])
        {
            [self deselectGraphicView:(TRGraphicView *)view];
        }
    }
}

# pragma mark - Motion
- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
}

@end




