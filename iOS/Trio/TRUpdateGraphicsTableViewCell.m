//
//  TRUpdateGraphicsTableViewCell.m
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRUpdateGraphicsTableViewCell.h"
#import "TRTheme.h"

@implementation TRUpdateGraphicsTableViewCell

- (void)awakeFromNib
{
    self.backdropView.backgroundColor = [TRTheme accentColor];
    self.backdropView.layer.cornerRadius = 5.0f;
    self.backdropView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.backdropView.backgroundColor = [TRTheme accentColor];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    self.backdropView.backgroundColor = [TRTheme accentColor];
}

@end
