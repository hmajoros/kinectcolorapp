//
//  TRGraphicView.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRGraphicView.h"

@implementation TRGraphicView

- (instancetype)initWithGraphic:(TRGraphic *)graphic
{
    CGRect frame = CGRectMake(0, 0, graphic.image.size.width, graphic.image.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        self.identifier = graphic.identifier;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:graphic.image];
        [imageView setContentMode:UIViewContentModeScaleToFill];
        [self updateImageContentView:imageView];
    }
    return self;
}

@end
