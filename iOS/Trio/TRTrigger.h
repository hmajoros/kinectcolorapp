//
//  TRTrigger.h
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

typedef enum {
    TRTriggerTypeFirstTouch = 1,
    TRTriggerTypeLongPress = 2,
    TRTriggerTypeDrag = 3
} TRTriggerType;

@interface TRTrigger : NSObject

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot;
- (instancetype)initWithType:(TRTriggerType)type;

@property (nonatomic) NSString *displayName;
@property (nonatomic) TRTriggerType type;

@end
