//
//  TRResultType.h
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

typedef enum
{
    TRResultTypeIndexUpdateGraphics = 1,
    TRResultTypeIndexSounds = 2,
    TRResultTypeIndexBackgroundMusic = 5
}TRResultTypeIndex;

@interface TRResultType : NSObject

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot;

@property (nonatomic) NSString *key;
@property (nonatomic) NSString *displayName;
@property (nonatomic) TRResultTypeIndex index;


@end
