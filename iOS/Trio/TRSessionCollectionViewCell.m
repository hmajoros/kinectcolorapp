//
//  TRSessionCollectionViewCell.m
//  Trio
//
//  Created by J. Christian Bator on 12/18/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRSessionCollectionViewCell.h"

@implementation TRSessionCollectionViewCell

- (void)awakeFromNib
{
    self.sessionImageView.layer.cornerRadius = 5.0f;
    self.sessionImageView.layer.masksToBounds = YES;
}

@end
