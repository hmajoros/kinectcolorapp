//
//  TRLibraryViewController.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRLibraryViewController.h"
#import "TRLibraryCollectionViewCell.h"
#import "TRLibrarySelectionViewController.h"
#import "TRLibraryDetailViewController.h"
#import "TRTheme.h"
#import "TRLibraryManager.h"
#import "TRWhiteboardManager.h"
#import "TRCanvasViewController.h"
#import "TRWhiteboardViewController.h"
#import "TRTitleViewController.h"

#define kTRCellIdentifier @"LibraryCell"

@interface TRLibraryViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, TRTitleViewControllerDelegate>

@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic) NSArray *libraryColors;
@property (nonatomic) NSArray *libraryContent;

@property (nonatomic) IBOutlet UICollectionView *libraryCollectionView;

@property (nonatomic) TRTitleViewController *titleViewController;

@end

@implementation TRLibraryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [TRTheme backgroundColor];
    self.libraryCollectionView.backgroundColor = [TRTheme backgroundColor];
    self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Library"];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style: UIBarButtonItemStylePlain target:nil action:nil];
    
    self.libraryContent = [TRLibraryManager sharedInstance].libraryCategories;
    self.libraryColors = [TRTheme colorPalette];
    
    [self.libraryCollectionView reloadData];
}

# pragma mark - Collection View
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.libraryContent.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TRLibraryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTRCellIdentifier forIndexPath:indexPath];
    
    cell.titleLabel.text = [self.libraryContent objectAtIndex:indexPath.row];
    cell.backgroundColor = [self.libraryColors objectAtIndex:indexPath.row];
    cell.layer.cornerRadius = 5.0f;
    cell.clipsToBounds = YES;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2)
    {
        self.selectedIndex = indexPath.row;
        [self performSegueWithIdentifier:@"libraryTOselection" sender:self];
    }
    else
    {
        self.selectedIndex = indexPath.row;
        [self performSegueWithIdentifier:@"libraryTOdetail" sender:self];
    }
}

# pragma mark - Collection view layout methods
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 12;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 12;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat widthOfScreen = self.view.bounds.size.width;
    CGFloat squareEdgeSize = (widthOfScreen - 36)/2;
    return CGSizeMake(squareEdgeSize, squareEdgeSize);
}

# pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"libraryTOdetail"])
    {
        TRLibraryDetailViewController *destination = segue.destinationViewController;
        switch (self.selectedIndex)
        {
            case 0:
                destination.detailType = TRLibraryDetailTypeBackgrounds;
                break;
            case 1:
                destination.detailType = TRLibraryDetailTypeGraphics;
                break;
            case 3:
                destination.detailType = TRLibraryDetailTypeResults;
                break;
            case 4:
                destination.detailType = TRLibraryDetailTypeMusic;
                break;
            default:
                break;
        }
    }
    else if ([segue.identifier isEqualToString:@"libraryTOselection"])
    {
        TRLibrarySelectionViewController *destination = segue.destinationViewController;
        switch (self.selectedIndex)
        {
            case 2:
                destination.detailType = TRLibraryDetailTypeTriggers;
                break;
            default:
                break;
        }
    }
}

# pragma mark - Custom Elements
- (void)presentCustomElementHandler
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        NSLog(@"Permission denied");
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // Get uplaods path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsPath = [paths objectAtIndex:0];
    
    NSURL *imageURL = info[UIImagePickerControllerReferenceURL];
    NSString *imageName = imageURL.path.lastPathComponent;
    
    NSString *s3UploadPath = [docsPath stringByAppendingPathComponent:imageName];

    // Get image data
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSData *data = UIImagePNGRepresentation(chosenImage);
    
    // Write to file
    [data writeToFile:s3UploadPath atomically:YES];
    
    // Upload
    [[TRLibraryManager sharedInstance] uploadCustomElement:[NSURL fileURLWithPath:s3UploadPath]];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

# pragma mark - Close canvas
- (IBAction)closePressed:(id)sender
{
    self.titleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TitleViewController"];
    self.titleViewController.delegate = self;
    [self.titleViewController showInView:self.splitViewController.view];
}

- (void)viewRemoved
{
    self.titleViewController = nil;
    [self.splitViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userSelectedTitle:(NSString *)title
{
    TRWhiteboardViewController *whiteboardViewController;
    UISplitViewController *splitController = self.splitViewController;
    UINavigationController *navigationController = [splitController.viewControllers objectAtIndex:1];
    TRCanvasViewController *canvasViewController = [navigationController.viewControllers objectAtIndex:0];
    
    for (UIViewController *viewController in canvasViewController.childViewControllers)
    {
        if ([viewController isKindOfClass:[TRWhiteboardViewController class]])
        {
            whiteboardViewController = (TRWhiteboardViewController *)viewController;
            break;
        }
    }

    if (whiteboardViewController)
    {
        [[TRWhiteboardManager sharedInstance] saveSessionWithEnvironment:whiteboardViewController.view andTitle:title];
    }
    [[TRWhiteboardManager sharedInstance] cleanupWhiteboard];
}

- (void)userCanceled
{
    [[TRWhiteboardManager sharedInstance] cleanupWhiteboard];
}

@end
