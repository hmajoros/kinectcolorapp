//
//  TRResult.m
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRResult.h"

@implementation TRResult

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot andResultType:(TRResultTypeIndex)index
{
    self = [super init];
    if (self)
    {
        self.index = index;
        self.key = snapshot.value;
        self.displayName = [snapshot.value stringByReplacingOccurrencesOfString:@"-" withString:@" "];
                
        switch (self.index)
        {
            case TRResultTypeIndexBackgroundMusic:
                self.result_id = self.index;
                break;
            case TRResultTypeIndexSounds:
                self.result_id = self.index;
                self.resourceCategory = @"Sounds";
                self.resourceFileName = snapshot.value;
                break;
            case TRResultTypeIndexUpdateGraphics:
            {
                if ([self.key isEqualToString:@"Delete-graphic"])
                {
                    self.result_id = 1;
                }
                else if ([self.key isEqualToString:@"Shrink-graphic"])
                {
                    self.result_id = 3;
                }
                else if ([self.key isEqualToString:@"Grow-graphic"])
                {
                    self.result_id = 4;
                }
                break;
            }
            default:
                break;
        }
    }
    return self;
}

- (instancetype)initWithType:(NSInteger)result_id resourceCategory:(NSString *)resourceCategory andResourceFileName:(NSString *)resourceFileName
{
    self = [super init];
    if (self)
    {
        self.result_id = result_id;
        switch (result_id)
        {
            case 1:
            {
                // delete
                self.index = 1;
                self.key = @"Delete-graphic";
                self.displayName = @"Delete graphic";
                break;
            }
            case 2:
            {
                // play sound
                self.index = result_id;
                self.resourceCategory = resourceCategory;
                self.resourceFileName = resourceFileName;
                break;
            }
            case 3:
            {
                // shrink
                self.index = 1;
                self.key = @"Shrink-graphic";
                self.displayName = @"Shrink graphic";
                break;
            }
            case 4:
            {
                //grow
                self.index = 1;
                self.key = @"Grow-graphic";
                self.displayName = @"Grow graphic";
                break;
            }
            case 5:
            {
                //toggle music
                self.index = result_id;
                self.key = @"Toggle";
                self.displayName = @"Toggle";
                break;
            }
            default:
                break;
        }
    }
    return self;
}

@end
