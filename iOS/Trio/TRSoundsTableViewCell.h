//
//  TRSoundsTableViewCell.h
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRSoundsTableViewCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel *titleLabel;

@end
