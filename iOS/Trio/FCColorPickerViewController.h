//
//  ColorPickerViewController.h
//  ColorPicker
//
//  Created by Fabián Cañas
//  Based on work by Gilly Dekel on 23/3/09
//  Copyright 2010-2014. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class FCColorPickerViewController, FCBrightDarkGradView, FCColorSwatchView;

@protocol FCColorPickerViewControllerDelegate <NSObject>

- (void)colorPickerViewController:(FCColorPickerViewController *)colorPicker didSelectColor:(UIColor *)color;

- (void)colorPickerViewControllerDidCancel:(FCColorPickerViewController *)colorPicker;

@end

@interface FCColorPickerViewController : UIViewController

+ (nonnull instancetype)colorPicker;

+ (instancetype)colorPickerWithColor:(nullable UIColor *)color delegate:(nullable id<FCColorPickerViewControllerDelegate>) delegate;

@property (readwrite, nonatomic, copy, nullable) UIColor *color;

@property (nonatomic, weak, nullable) id <FCColorPickerViewControllerDelegate> delegate;

@property (nonatomic, copy, nullable) UIColor *backgroundColor;

@property (nonatomic, copy, null_resettable) UIColor *tintColor;

@end

NS_ASSUME_NONNULL_END


