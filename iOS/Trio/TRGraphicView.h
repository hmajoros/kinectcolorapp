//
//  TRGraphicView.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRGraphic.h"
#import "SPUserResizableView.h"

@interface TRGraphicView : SPUserResizableView

@property (nonatomic) NSInteger identifier;

- (instancetype)initWithGraphic:(TRGraphic *)graphic;

@end
