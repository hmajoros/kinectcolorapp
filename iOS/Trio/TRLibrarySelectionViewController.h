//
//  TRLibrarySelectionViewController.h
//  Trio
//
//  Created by J. Christian Bator on 12/10/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRLibraryManager.h"
#import "TRCategory.h"
#import "TRResultType.h"

@interface TRLibrarySelectionViewController : UIViewController

@property (nonatomic) TRLibraryDetailType detailType;
@property (nonatomic) TRCategory *category;
@property (nonatomic) TRResultType *type;

@end
