//
//  TRBackground.m
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRBackground.h"

@implementation TRBackground

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot
{
    self = [super init];
    if (self)
    {
        self.filename = snapshot.value;
    }
    return self;
}

@end
