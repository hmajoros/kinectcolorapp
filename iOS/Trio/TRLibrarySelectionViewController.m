//
//  TRLibrarySelectionViewController.m
//  Trio
//
//  Created by J. Christian Bator on 12/10/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRLibrarySelectionViewController.h"
#import "TRWhiteboardManager.h"
#import "TRTheme.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageManager.h>

#import "TRGraphic.h"
#import "TRBackground.h"
#import "TRTrigger.h"
#import "TRResult.h"
#import "TRMusic.h"

#import "TRGraphicsTableViewCell.h"
#import "TRTriggersTableViewCell.h"
#import "TRSoundsTableViewCell.h"
#import "TRUpdateGraphicsTableViewCell.h"

#define kTRGraphicsCellIdentifier @"GraphicsCell"
#define kTRMusicCellIdentifier @"MusicCell"
#define kTRTriggersCellIdentifier @"TriggerCell"
#define kTRSoundCellIdentifier @"SoundCell"
#define kTRUpdateGraphicsCellIdentifier @"UpdateGraphicsCell"

#define kTRAWSBaseUrl @"https://s3.amazonaws.com/triotriotrio"

@interface TRLibrarySelectionViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic) NSArray *dataSource;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic) NSInteger graphicID;
@property (nonatomic) IBOutlet UITableView *selectionTableView;

@end

@implementation TRLibrarySelectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [TRTheme backgroundColor];
    self.selectionTableView.backgroundColor = [TRTheme backgroundColor];
    self.selectionTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (self.category)
    {
        self.navigationItem.titleView = [TRTheme navigationTitleLabel:self.category.displayName];
    }
    else if (self.type)
    {
        self.navigationItem.titleView = [TRTheme navigationTitleLabel:self.type.displayName];
    }
    else
    {
        self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Triggers"];
    }
    
 
    switch (self.detailType)
    {
        case TRLibraryDetailTypeBackgrounds:
            [self loadBackgroundsData];
            break;
        case TRLibraryDetailTypeGraphics:
            [self loadGraphicsData];
            break;
        case TRLibraryDetailTypeTriggers:
            [self loadTriggersData];
            break;
        case TRLibraryDetailTypeResults:
            [self loadResultsData];
            break;
        case TRLibraryDetailTypeMusic:
            [self loadMusicData];
            break;
        default:
            break;
    }
}

# pragma mark - Data Sources
- (void)loadBackgroundsData
{
    [[TRLibraryManager sharedInstance] getBackgroundsForCategory:self.category.key withBlock:^(NSArray *backgrounds)
    {
        self.dataSource = backgrounds;
        [self.selectionTableView reloadData];
    }];
}

- (void)loadGraphicsData
{
    [[TRLibraryManager sharedInstance] getGraphicsForCategory:self.category.key withBlock:^(NSArray *graphics)
     {
         self.dataSource = graphics;
         [self.selectionTableView reloadData];
     }];
}

- (void)loadTriggersData
{
    [self.navigationItem.rightBarButtonItem setEnabled:[TRWhiteboardManager sharedInstance].actionMode];
    self.graphicID = [TRWhiteboardManager sharedInstance].selectedGraphicID;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchMode:) name:@"switch_mode" object:nil];
    
    [[TRLibraryManager sharedInstance] getTriggersWithBlock:^(NSArray *triggers)
     {
         self.dataSource = triggers;
         [self.selectionTableView reloadData];
     }];
}

- (void)loadResultsData
{
    [self.navigationItem.rightBarButtonItem setEnabled:[TRWhiteboardManager sharedInstance].actionMode];
    self.graphicID = [TRWhiteboardManager sharedInstance].selectedGraphicID;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchMode:) name:@"switch_mode" object:nil];
    
    [[TRLibraryManager sharedInstance] getResultsForType:self.type withBlock:^(NSArray *results)
     {
         self.dataSource = results;
         [self.selectionTableView reloadData];
     }];
}

- (void)loadMusicData
{
    [[TRLibraryManager sharedInstance] getMusicForCategory:self.category.key withBlock:^(NSArray *music)
     {
         self.dataSource = music;
         [self.selectionTableView reloadData];
     }];
}

# pragma mark - Selection notifications
- (void)switchMode:(NSNotification *)notification
{
    NSDictionary *user_info = notification.userInfo;
    BOOL actionMode = [[user_info objectForKey:@"action_mode"] boolValue];
    [self.navigationItem.rightBarButtonItem setEnabled:actionMode];

    if (actionMode)
    {
        self.graphicID = [[user_info objectForKey:@"graphic_id"] integerValue];
    }
    else
    {
        self.graphicID = NSIntegerMin;
    }
}

# pragma mark - Table View Datasource and Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.detailType)
    {
        case TRLibraryDetailTypeBackgrounds:
            return 240;
            break;
        case TRLibraryDetailTypeGraphics:
            return 240;
            break;
        case TRLibraryDetailTypeTriggers:
            return 164;
            break;
        case TRLibraryDetailTypeResults:
            return 164;
            break;
        case TRLibraryDetailTypeMusic:
          return 240;
            break;
        default:
            return 240;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.detailType)
    {
        case TRLibraryDetailTypeBackgrounds:
        {
            TRGraphicsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRGraphicsCellIdentifier];
            
            TRBackground *background = [self.dataSource objectAtIndex:indexPath.row];
            NSString *url_string = [NSString stringWithFormat:@"%@/backgrounds/%@/%@", kTRAWSBaseUrl, self.category.key, background.filename];
            [cell.graphicsImageView sd_setImageWithURL:[NSURL URLWithString:url_string]];
            return cell;
            
            break;
        }
        case TRLibraryDetailTypeGraphics:
        {
            TRGraphicsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRGraphicsCellIdentifier];
            
            TRGraphic *graphic = [self.dataSource objectAtIndex:indexPath.row];
            NSString *url_string = [NSString stringWithFormat:@"%@/graphics/%@/%@", kTRAWSBaseUrl, self.category.key, graphic.filename];
            [cell.graphicsImageView sd_setImageWithURL:[NSURL URLWithString:url_string]];
            return cell;
            break;
        }
        case TRLibraryDetailTypeTriggers:
        {
            TRTriggersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRTriggersCellIdentifier];
            
            TRTrigger *trigger = [self.dataSource objectAtIndex:indexPath.row];
            cell.titleLabel.text = trigger.displayName;
            cell.triggerImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"trigger_%ld", (long)trigger.type]];
            return cell;
            break;
        }
        case TRLibraryDetailTypeResults:
        {
            switch (self.type.index)
            {
                case TRResultTypeIndexSounds:
                {
                    TRSoundsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRSoundCellIdentifier];
                    
                    TRResult *result = [self.dataSource objectAtIndex:indexPath.row];
                    cell.titleLabel.text = result.displayName;
                    return cell;
                    break;
                }
                case TRResultTypeIndexUpdateGraphics:
                {
                    TRUpdateGraphicsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRUpdateGraphicsCellIdentifier];
                    
                    TRResult *result = [self.dataSource objectAtIndex:indexPath.row];
                    cell.titleLabel.text = result.displayName;
                    cell.updateGraphicImageView.image = [UIImage imageNamed:result.key];
                    return cell;

                    break;
                }
                case TRResultTypeIndexBackgroundMusic:
                {
                    TRUpdateGraphicsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRUpdateGraphicsCellIdentifier];
                    
                    TRResult *result = [self.dataSource objectAtIndex:indexPath.row];
                    cell.titleLabel.text = result.displayName;
                    cell.updateGraphicImageView.image = [UIImage imageNamed:result.key];
                    return cell;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case TRLibraryDetailTypeMusic:
        {
            TRSoundsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTRSoundCellIdentifier];
            
            TRMusic *music = [self.dataSource objectAtIndex:indexPath.row];
            cell.titleLabel.text = music.displayName;
            return cell;
            break;
        }
        default:
            break;
    }
    
    return [UITableViewCell new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndex = indexPath.row;
}

# pragma mark - IBActions
- (IBAction)addPressed:(id)sender
{
    switch (self.detailType)
    {
        case TRLibraryDetailTypeBackgrounds:
        {
            TRBackground *background = [self.dataSource objectAtIndex:self.selectedIndex];
            
            NSString *url_string = [NSString stringWithFormat:@"%@/backgrounds/%@/%@", kTRAWSBaseUrl, self.category.key, background.filename];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:url_string] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
             {
                 if (image)
                 {
                     [[TRWhiteboardManager sharedInstance] updateBackgroundImageWithName:background.filename andCategory:self.category.key andImage:image];
                 }
             }];
            
            break;
        }
        case TRLibraryDetailTypeGraphics:
        {
            TRGraphic *original_graphic = [self.dataSource objectAtIndex:self.selectedIndex];
            
            TRGraphic *graphic = [TRGraphic createGraphicFromCopy:original_graphic];
            [graphic updateIdentifier];
            
            NSString *url_string = [NSString stringWithFormat:@"%@/graphics/%@/%@", kTRAWSBaseUrl, self.category.key, graphic.filename];
            
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            [manager downloadImageWithURL:[NSURL URLWithString:url_string] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL)
            {
                    if (image)
                    {
                        graphic.image = image;
                        [[TRWhiteboardManager sharedInstance] addGraphic:graphic];
                    }
            }];
            break;
        }
        case TRLibraryDetailTypeTriggers:
        {
            TRTrigger *trigger = [self.dataSource objectAtIndex:self.selectedIndex];
            [[TRWhiteboardManager sharedInstance] addTrigger:trigger toGraphicWithIdentifier:self.graphicID];
            break;
        }
        case TRLibraryDetailTypeResults:
        {
            TRResult *result = [self.dataSource objectAtIndex:self.selectedIndex];
            [[TRWhiteboardManager sharedInstance] addResult:result toGraphicWithIdentifier:self.graphicID];
            break;
        }
        case TRLibraryDetailTypeMusic:
        {
            [TRWhiteboardManager sharedInstance].background_music_category = self.category.key;
            [TRWhiteboardManager sharedInstance].background_music_filename = ((TRMusic *)[self.dataSource objectAtIndex:self.selectedIndex]).filename;
            break;
        }
        default:
            break;
    }
}


@end
