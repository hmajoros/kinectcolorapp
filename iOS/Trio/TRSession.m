//
//  TRSession.m
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRSession.h"

@implementation TRSession

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot
{
    self = [super init];
    if (self)
    {
        self.sessionID = snapshot.key;
        self.title = snapshot.value[@"title"];
        self.environment = snapshot.value[@"environment"];
       
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsPath = [paths objectAtIndex:0];
        NSString *dataPath = [docsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", self.sessionID]];
        self.image = [UIImage imageWithContentsOfFile:dataPath];
    }
    return self;
}

@end
