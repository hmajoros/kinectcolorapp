//
//  TRResultType.m
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRResultType.h"

@implementation TRResultType

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot
{
    self = [super init];
    if (self)
    {
        self.key = snapshot.key;
        self.displayName = [snapshot.key stringByReplacingOccurrencesOfString:@"-" withString:@" "];
        self.index = [snapshot.value integerValue];
    }
    return self;
}

@end
