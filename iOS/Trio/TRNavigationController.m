//
//  TRNavigationController.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRNavigationController.h"
#import "TRTheme.h"

@interface TRNavigationController ()

@end

@implementation TRNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationBar setBarTintColor:[TRTheme accentColor]];
    [self.navigationBar setTintColor:[TRTheme backgroundColor]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
