//
//  SPUserResizableView.h
//  Trio
//
//  Created by J. Christian Bator on 12/15/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef struct SPUserResizableViewAnchorPoint
{
    CGFloat adjustsX;
    CGFloat adjustsY;
    CGFloat adjustsH;
    CGFloat adjustsW;
} SPUserResizableViewAnchorPoint;

@interface SPGripViewBorderView : UIView
@end

@interface SPUserResizableView : UIControl

@property (nonatomic) UIImageView *imageContentView;
@property (nonatomic) SPGripViewBorderView *borderView;
@property (nonatomic) CGPoint touchStart;
@property (nonatomic) CGFloat minWidth;
@property (nonatomic) CGFloat minHeight;

@property (nonatomic) SPUserResizableViewAnchorPoint anchorPoint;
@property (nonatomic) BOOL preventsPositionOutsideSuperview;

- (void)hideEditingHandles;
- (void)showEditingHandles;
- (void)updateImageContentView:(UIImageView *)newContentView;

@end

