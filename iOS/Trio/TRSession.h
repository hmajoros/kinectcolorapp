//
//  TRSession.h
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>

@interface TRSession : NSObject

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot;

@property (nonatomic) NSString *sessionID;
@property (nonatomic) NSString *title;
@property (nonatomic) UIImage *image;
@property (nonatomic) NSDictionary *environment;

@end
