//
//  TRLibraryDetailViewController.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRLibraryManager.h"

@interface TRLibraryDetailViewController : UIViewController

@property (nonatomic) TRLibraryDetailType detailType;

@end
