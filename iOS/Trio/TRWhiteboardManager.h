//
//  TRWhiteboardManager.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRGraphic.h"
#import "TRTrigger.h"
#import "TRResult.h"
#import "TRSession.h"

@protocol TRWhiteboardDelegate <NSObject>
@required
- (void)backgroundColorUpdatedWithColor:(UIColor *)color;
- (void)backgroundImageUpdatedWithImage:(UIImage *)image;
- (void)graphicAdded:(TRGraphic *)graphic;
- (void)graphicRemoved:(TRGraphic *)graphic;
@end

@interface TRWhiteboardManager : NSObject
{
    __weak id <TRWhiteboardDelegate> _delegate;
}

@property (nonatomic, weak) id delegate;

@property (nonatomic) NSInteger pin;

@property (nonatomic) BOOL actionMode;
@property (nonatomic) NSInteger selectedGraphicID;
@property (nonatomic) NSMutableArray *selectedGraphicIdentifiers;

@property (nonatomic) NSMapTable *graphics;
@property (nonatomic) UIColor *background_color;
@property (nonatomic) NSString *background_image_name;
@property (nonatomic) NSString *background_category_key;

@property (nonatomic) NSString *background_music_category;
@property (nonatomic) NSString *background_music_filename;

@property (nonatomic) TRSession *session;

# pragma mark - Initialization
+ (instancetype)sharedInstance;
- (void)initWhiteboard;
- (void)prepareWhiteboardForNewEnvironment;
- (void)prepareWhiteboardFromExistingEnvironmentAtIndex:(NSInteger)index;
- (void)cleanupWhiteboard;

# pragma mark - User session handling
- (NSInteger)getSessionCount;
- (TRSession *)getSessionAtIndex:(NSInteger)index;
- (void)saveSessionWithEnvironment:(UIView *)environment andTitle:(NSString *)title;

# pragma mark - Set Background Images and Colors
- (void)updateBackgroundImageWithName:(NSString *)image_name andCategory:(NSString *)category_key andImage:(UIImage *)image;
- (void)updateBackgroundColor:(UIColor *)color;

# pragma mark - Graphics
- (TRGraphic *)getGraphicWithIdentifier:(NSInteger)identifier;
- (void)addGraphic:(TRGraphic *)graphic;
- (void)deleteSelectedGraphics;

# pragma mark - Events
- (void)addTrigger:(TRTrigger *)trigger toGraphicWithIdentifier:(NSInteger)graphicIdentifier;
- (void)addResult:(TRResult *)result toGraphicWithIdentifier:(NSInteger)graphicIdentifier;
- (void)removeEventAtIndex:(NSInteger)index fromGraphicWithIdentifier:(NSInteger)graphicIdentifier;

# pragma mark - Sending
- (BOOL)sendEnvironment:(UIView *)environment;

# pragma mark - Removing
- (void)removeEnvironment;

@end
