//
//  TRLibraryManager.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TRResultType.h"

typedef enum
{
    TRLibraryDetailTypeBackgrounds,
    TRLibraryDetailTypeGraphics,
    TRLibraryDetailTypeTriggers,
    TRLibraryDetailTypeResults,
    TRLibraryDetailTypeMusic
}TRLibraryDetailType;

@interface TRLibraryManager : NSObject

+ (instancetype)sharedInstance;
- (void)initLibrary;
- (void)initAWS;

# pragma mark - Categories
- (NSArray *)libraryCategories;

# pragma mark - Category Detail Lists
- (void)getBackgroundCategoriesWithBlock:(void (^)(NSArray *))block;
- (void)getGraphicCategoriesWithBlock:(void (^)(NSArray *))block;
- (void)getResultTypesWithBlock:(void (^)(NSArray *))block;
- (void)getMusicCategoriesWithBlock:(void (^)(NSArray *))block;

# pragma mark - Categorical Element Lists
- (void)getBackgroundsForCategory:(NSString *)category withBlock:(void (^)(NSArray *))block;
- (void)getGraphicsForCategory:(NSString *)category withBlock:(void (^)(NSArray *))block;
- (void)getTriggersWithBlock:(void (^)(NSArray *))block;
- (void)getResultsForType:(TRResultType *)type withBlock:(void (^)(NSArray *))block;
- (void)getMusicForCategory:(NSString *)category withBlock:(void (^)(NSArray *))block;

# pragma mark - Custom Elements
- (void)uploadCustomElement:(NSURL *)bodyUrl;

@end
