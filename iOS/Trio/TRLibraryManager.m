//
//  TRLibraryManager.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRLibraryManager.h"
#import <Firebase/Firebase.h>
#import "AWSS3.h"
#import "TRCategory.h"
#import "TRResultType.h"
#import "TRBackground.h"
#import "TRGraphic.h"
#import "TRTrigger.h"
#import "TRResult.h"
#import "TRMusic.h"

@interface TRLibraryManager ()
{
    Firebase *libraryRef;
    
    // Categories
    NSMutableArray *categories;
    
    // Category Detail Lists
    NSMutableArray *backgroundCategories;
    NSMutableArray *graphicCategories;
    NSMutableArray *resultTypes;
    NSMutableArray *musicCategories;
}
@end

@implementation TRLibraryManager

# pragma mark - Initializers
+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^
      {
          sharedInstance = [[self alloc] init];
      });
    return sharedInstance;
}

- (void)initLibrary
{
    // Data init
    categories = [@[@"Backgrounds", @"Graphics", @"Triggers", @"Results", @"Music"] mutableCopy];
    
    // Ref init
    libraryRef = [[Firebase alloc] initWithUrl:@"https://triotriotrio.firebaseio.com/library"];
    [libraryRef keepSynced:YES];
}

- (void)initAWS
{
    NSString *AWS_IDENTITY = @"us-east-1:a87a9514-e1c6-4730-828b-26ea4385f524";
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:AWS_IDENTITY];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
}

# pragma mark - Categories
- (NSArray *)libraryCategories
{
    return categories;
}

# pragma mark - Category Detail Lists
- (void)getBackgroundCategoriesWithBlock:(void (^)(NSArray *))block
{
    if (backgroundCategories)
    {
        block(backgroundCategories);
    }
    else
    {
        backgroundCategories = [NSMutableArray array];
        [[libraryRef childByAppendingPath:@"backgrounds/categories"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             NSMutableArray *data = [NSMutableArray array];
             for (FDataSnapshot *child in snapshot.children)
             {
                 [data addObject:[[TRCategory alloc] initWithSnapshot:child]];
             }
             backgroundCategories = [data mutableCopy];
             block(backgroundCategories);
         }];
    }
}

- (void)getGraphicCategoriesWithBlock:(void (^)(NSArray *))block
{
    if (graphicCategories)
    {
        block(graphicCategories);
    }
    else
    {
        graphicCategories = [NSMutableArray array];
        [[libraryRef childByAppendingPath:@"graphics/categories"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             for (FDataSnapshot *child in snapshot.children)
             {
                 [graphicCategories addObject:[[TRCategory alloc] initWithSnapshot:child]];
             }
             block(graphicCategories);
         }];
    }
}

- (void)getResultTypesWithBlock:(void (^)(NSArray *))block
{
    if (resultTypes)
    {
        block(resultTypes);
    }
    else
    {
        resultTypes = [NSMutableArray array];
        [[libraryRef childByAppendingPath:@"results/categories"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             for (FDataSnapshot *child in snapshot.children)
             {
                 [resultTypes addObject:[[TRResultType alloc] initWithSnapshot:child]];
             }
             block(resultTypes);
         }];
    }
}

- (void)getMusicCategoriesWithBlock:(void (^)(NSArray *))block
{
    if (musicCategories)
    {
        block(musicCategories);
    }
    else
    {
        musicCategories = [NSMutableArray array];
        [[libraryRef childByAppendingPath:@"music/categories"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
         {
             for (FDataSnapshot *child in snapshot.children)
             {
                 [musicCategories addObject:[[TRCategory alloc] initWithSnapshot:child]];
             }
             block(musicCategories);
         }];
    }
}

# pragma mark - Categorical Element Lists
- (void)getBackgroundsForCategory:(NSString *)category withBlock:(void (^)(NSArray *))block
{
    NSMutableArray *graphics = [NSMutableArray array];
    [[libraryRef childByAppendingPath:[NSString stringWithFormat:@"backgrounds/data/%@", category]] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
     {
         for (FDataSnapshot *child in snapshot.children)
         {
             [graphics addObject:[[TRGraphic alloc] initWithSnapshot:child andCategory:category]];
         }
         block(graphics);
     }];
}

- (void)getGraphicsForCategory:(NSString *)category withBlock:(void (^)(NSArray *))block
{
    NSMutableArray *graphics = [NSMutableArray array];
    [[libraryRef childByAppendingPath:[NSString stringWithFormat:@"graphics/data/%@", category]] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
     {
         for (FDataSnapshot *child in snapshot.children)
         {
             [graphics addObject:[[TRGraphic alloc] initWithSnapshot:child andCategory:category]];
         }
         block(graphics);
     }];
}

- (void)getTriggersWithBlock:(void (^)(NSArray *))block
{
    NSMutableArray *triggers = [NSMutableArray array];
    [[libraryRef childByAppendingPath:@"triggers/data"] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
     {
         for (FDataSnapshot *child in snapshot.children)
         {
             TRTrigger *trigger = [[TRTrigger alloc] initWithSnapshot:child];
             [triggers addObject:trigger];
         }
         block(triggers);
     }];
}

- (void)getResultsForType:(TRResultType *)type withBlock:(void (^)(NSArray *))block
{
    NSMutableArray *results = [NSMutableArray array];
    [[libraryRef childByAppendingPath:[NSString stringWithFormat:@"results/data/%@", type.key]] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
     {
         for (FDataSnapshot *child in snapshot.children)
         {
             [results addObject:[[TRResult alloc] initWithSnapshot:child andResultType:type.index]];
         }
         block(results);
     }];
}

- (void)getMusicForCategory:(NSString *)category withBlock:(void (^)(NSArray *))block
{
    NSMutableArray *music = [NSMutableArray array];
    [[libraryRef childByAppendingPath:[NSString stringWithFormat:@"music/data/%@", category]] observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot)
     {
         for (FDataSnapshot *child in snapshot.children)
         {
             TRMusic *music_instance = [[TRMusic alloc] initWithSnapshot:child];
             [music addObject:music_instance];
         }
         block(music);
     }];
}

# pragma mark - Custom Elements
- (void)uploadCustomElement:(NSURL *)bodyUrl
{
    AWSS3TransferManager *transfer_manager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *element_upload_request = [AWSS3TransferManagerUploadRequest new];
    element_upload_request.bucket = @"triotriotrio";
    
    NSString *timestamp = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
   
    NSString *key = [NSString stringWithFormat:@"graphics/Custom/%@.png", timestamp];
    
    element_upload_request.key = key;
    element_upload_request.body = bodyUrl;
    element_upload_request.contentType = @"image/png";
    
    [[transfer_manager upload:element_upload_request] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task)
     {
         if (task.error)
         {
             NSLog(@"Error uploading");
         }
         else if (task.result)
         {
             NSLog(@"Success uploading");
         }
         
         return nil;
     }];

}

@end
