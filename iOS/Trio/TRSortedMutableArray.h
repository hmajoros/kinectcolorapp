//
//  TRSortedMutableArray.h
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TRSortedMutableArray : NSArray

- (id)initWithComparator:(NSComparator)comparator;

- (NSUInteger)addObject:(id)obj;
- (void)removeObjectAtIndex:(NSUInteger)index;
- (void)removeAllObjects;
- (void)sort;

@end
