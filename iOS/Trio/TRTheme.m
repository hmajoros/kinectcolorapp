//
//  TRTheme.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRTheme.h"

@implementation TRTheme

+ (UIColor *)accentColor
{
    return [UIColor colorWithRed:39.0/255.0 green:174.0/255.0 blue:96.0/255.0 alpha:1.0];
}

+ (UIColor *)backgroundColor
{
    return [UIColor whiteColor];
}

+ (NSArray *)colorPalette
{
    return @[
             [UIColor colorWithRed:34.0/255.0 green:38.0/255.0 blue:197.0/255.0 alpha:1.0],
             [UIColor colorWithRed:187.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1.0],
             [UIColor colorWithRed:250.0/255.0 green:175.0/255.0 blue:44.0/255.0 alpha:1.0],
             [UIColor colorWithRed:34.0/255.0 green:38.0/255.0 blue:197.0/255.0 alpha:1.0],
             [UIColor colorWithRed:187.0/255.0 green:33.0/255.0 blue:33.0/255.0 alpha:1.0],
             [UIColor colorWithRed:250.0/255.0 green:175.0/255.0 blue:44.0/255.0 alpha:1.0]
             ];
}

+ (UIColor *)canvasColor
{
    return [UIColor colorWithRed:0.85 green:0.85 blue:0.85 alpha:1.0];
}

+ (UIColor *)neutralColor
{
    return [UIColor colorWithWhite:0.67 alpha:1.0];
}

+ (UIColor *)disabledColor
{
    return [UIColor colorWithWhite:0.85 alpha:1.0];
}

+ (UILabel *)navigationTitleLabel:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = self.navigationTitleFont;
    label.textColor = self.backgroundColor;
    label.text = title;
    [label sizeToFit];
    
    return label;
}

+ (NSString *)hexStringFromColor:(UIColor *)color
{
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX",
            lroundf(r * 255),
            lroundf(g * 255),
            lroundf(b * 255)];
}

+ (UIFont *)navigationTitleFont
{
    return [UIFont fontWithName:@"AvenirNext-Medium" size:20];
}

@end
