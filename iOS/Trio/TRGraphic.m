//
//  TRGraphic.m
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRGraphic.h"
#import "TRTrigger.h"
#import "TRResult.h"

static unsigned long unique_id = 0;

@implementation TRGraphic

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot andCategory:(NSString *)category
{
    self = [super init];
    if (self)
    {
        self.filename = snapshot.value;
        self.category = category;
    }
    return self;
}

- (instancetype)initWithJSON:(NSDictionary *)json
{
    self = [super init];
    if (self)
    {
        self.filename = json[@"filename"];
        self.category = json[@"category"];
        self.identifier = unique_id++;
        self.x_ratio = [json[@"x_ratio"] doubleValue];
        self.y_ratio = [json[@"y_ratio"] doubleValue];
        self.width_ratio = [json[@"width_ratio"] doubleValue];
        self.height_ratio = [json[@"height_ratio"] doubleValue];
        
        self.actions = [NSMutableArray array];
        for (NSDictionary *event in json[@"events"])
        {
            TRTriggerType trigger_type = [event[@"trigger"] integerValue];
            [self.actions addObject:[[TRTrigger alloc] initWithType:trigger_type]];
            
            if (!(trigger_type == TRTriggerTypeDrag))
            {
                TRResultTypeIndex result_type = [event[@"result"] integerValue];
                [self.actions addObject:[[TRResult alloc] initWithType:result_type resourceCategory:event[@"resource_category"] andResourceFileName:event[@"resource_filename"]]];
            }
        }
    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    if (self.identifier == ((TRGraphic *)(object)).identifier)
    {
        return YES;
    }
    
    return NO;
}

- (void)updateIdentifier
{
    self.identifier = unique_id++;
}

- (NSDictionary *)toJSON
{
    NSInteger trigger_index = -1;
    NSMutableArray *events = [NSMutableArray array];
    for (id event in self.actions)
    {
        NSMutableDictionary *event_dict = [NSMutableDictionary dictionary];
        if ([event isKindOfClass:[TRTrigger class]])
        {
            TRTrigger *trigger = (TRTrigger *)event;
            [event_dict setObject:@(trigger.type) forKey:@"trigger"];
            [events addObject:event_dict];
            trigger_index++;
        }
        else if (([event isKindOfClass:[TRResult class]]) && (trigger_index > -1))
        {
            TRResult *result = (TRResult *)event;
            [events[trigger_index] setObject:@(result.result_id) forKey:@"result"];
            
            if (result.resourceFileName && result.resourceCategory)
            {
                [events[trigger_index] setObject:[result.resourceCategory lowercaseString] forKey:@"resource_category"];
                [events[trigger_index] setObject:result.resourceFileName forKey:@"resource_filename"];
            }
        }
    }

    return @{
             @"type" : @"graphics",
             @"category" : self.category,
             @"filename" : self.filename,
             @"x_ratio" : @(self.x_ratio),
             @"y_ratio" : @(self.y_ratio),
             @"width_ratio" : @(self.width_ratio),
             @"height_ratio" : @(self.height_ratio),
             @"events" : events
             };
}

+ (TRGraphic *)createGraphicFromCopy:(TRGraphic *)inGraphic
{
    TRGraphic *graphic = [[TRGraphic alloc] init];
    graphic.filename = inGraphic.filename;
    graphic.category = inGraphic.category;
    graphic.actions = [NSMutableArray array];
    return graphic;
}

@end
