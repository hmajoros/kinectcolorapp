//
//  TRTitleViewController.h
//  Trio
//
//  Created by J. Christian Bator on 12/18/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TRTitleViewControllerDelegate <NSObject>
@required
- (void)userSelectedTitle:(NSString *)title;
- (void)userCanceled;
- (void)viewRemoved;
@end


@interface TRTitleViewController : UIViewController
{
    __weak id <TRTitleViewControllerDelegate> _delegate;
}

@property (nonatomic, weak) id delegate;

- (void)showInView:(UIView *)parent_view;

@end

