//
//  TRTheme.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface TRTheme : NSObject

+ (UIColor *)accentColor;
+ (UIColor *)backgroundColor;
+ (NSArray *)colorPalette;
+ (UIColor *)canvasColor;
+ (UIColor *)neutralColor;
+ (UIColor *)disabledColor;
+ (NSString *)hexStringFromColor:(UIColor *)color;

+ (UILabel *)navigationTitleLabel:(NSString *)title;

@end
