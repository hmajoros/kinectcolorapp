//
//  TRSortedMutableArray.m
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRSortedMutableArray.h"

@interface TRSortedMutableArray()

@property (nonatomic, strong) NSMutableArray *backingArray;
@property (nonatomic, strong) NSComparator comparator;

@end

@implementation TRSortedMutableArray

@synthesize backingArray = _backingArray;
@synthesize comparator = _comparator;

# pragma mark -
- (id)init {
    if ((self = [super init]))
    {
        self.backingArray = [NSMutableArray new];
    }
    return self;
}

- (id)initWithComparator:(NSComparator)comparator
{
    if ((self = [self init]))
    {
        self.comparator = comparator;
    }
    return self;
}

# pragma mark -
- (NSUInteger)addObject:(id)obj
{
    NSUInteger addedIndex = [_backingArray indexOfObject:obj inSortedRange:NSMakeRange(0, _backingArray.count) options:NSBinarySearchingInsertionIndex usingComparator:_comparator];
    [_backingArray insertObject:obj atIndex:addedIndex];
    return addedIndex;
}

- (void)removeObjectAtIndex:(NSUInteger)index
{
    [_backingArray removeObjectAtIndex:index];
}

- (void)removeAllObjects
{
    [_backingArray removeAllObjects];
}

# pragma mark - NSArray primitives
- (id)objectAtIndex:(NSUInteger)index
{
    return [_backingArray objectAtIndex:index];
}

- (NSUInteger)count
{
    return _backingArray.count;
}

- (BOOL)containsObject:(id)anObject
{
    return [_backingArray containsObject:anObject];
}

# pragma mark - NSArray overridden methods
- (NSUInteger)indexOfObject:(id)anObject
{
    return [_backingArray indexOfObject:anObject];
}

# pragma mark - Block methods
- (void)enumerateObjectsUsingBlock:(void (^)(id obj, NSUInteger idx, BOOL *stop))block {
    [_backingArray enumerateObjectsUsingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
        block(obj2, idx2, stop2);
    }];
}

- (void)enumerateObjectsWithOptions:(NSEnumerationOptions)opts usingBlock:(void (^)(id obj, NSUInteger idx, BOOL *stop))block {
    [_backingArray enumerateObjectsWithOptions:opts
                                    usingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                                        block(obj2, idx2, stop2);
                                    }];
}

- (void)enumerateObjectsAtIndexes:(NSIndexSet *)s options:(NSEnumerationOptions)opts usingBlock:(void (^)(id obj, NSUInteger idx, BOOL *stop))block {
    [_backingArray enumerateObjectsAtIndexes:s
                                     options:opts
                                  usingBlock:^(id obj2, NSUInteger idx2, BOOL *stop2) {
                                      block(obj2, idx2, stop2);
                                  }];
}

# pragma mark - NSFastEnumeration
- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(__unsafe_unretained id [])buffer count:(NSUInteger)len {
    return [_backingArray countByEnumeratingWithState:state objects:buffer count:len];
}

# pragma mark - Sorting
- (void)sort
{
    [_backingArray sortUsingComparator:_comparator];
}

@end
