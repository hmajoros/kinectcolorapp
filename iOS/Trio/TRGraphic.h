//
//  TRGraphic.h
//  Trio
//
//  Created by J. Christian Bator on 10/20/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>

@interface TRGraphic : NSObject

// Local
@property (nonatomic) NSInteger identifier;
@property (nonatomic) UIImage *image;

// Shared
@property (nonatomic) NSString *filename;
@property (nonatomic) NSString *category;

@property (nonatomic) double x_ratio;
@property (nonatomic) double y_ratio;
@property (nonatomic) double width_ratio;
@property (nonatomic) double height_ratio;
@property (nonatomic) NSMutableArray *actions;

- (void)updateIdentifier;
- (NSDictionary *)toJSON;

- (instancetype)initWithJSON:(NSDictionary *)json;
- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot andCategory:(NSString *)category;
+ (TRGraphic *)createGraphicFromCopy:(TRGraphic *)inGraphic;

@end
