//
//  TRMenuViewController.m
//  Trio
//
//  Created by J. Christian Bator on 12/17/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import "TRMenuViewController.h"
#import "TRTheme.h"
#import "TRWhiteboardManager.h"
#import "TRLibraryManager.h"
#import "TRSessionCollectionViewCell.h"

@interface TRMenuViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation TRMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [TRTheme navigationTitleLabel:@"Trio"];
    
    [[TRLibraryManager sharedInstance] initLibrary];
    [[TRLibraryManager sharedInstance] initAWS];
    [[TRWhiteboardManager sharedInstance] initWhiteboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:@"sessions_loaded" object:nil];
}

- (IBAction)newPressed:(id)sender
{
    [[TRWhiteboardManager sharedInstance] prepareWhiteboardForNewEnvironment];
    [self performSegueWithIdentifier:@"menuTOcanvas" sender:self];
}

# pragma mark - Data Listener
- (void)reloadData
{
    [self.collectionView reloadData];
}

# pragma mark - Collection View
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 16;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 16;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat widthOfScreen = self.view.bounds.size.width;
    CGFloat squareEdgeSize = (widthOfScreen - 56)/3;
    return CGSizeMake(squareEdgeSize, squareEdgeSize * 3.5 / 4.0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[TRWhiteboardManager sharedInstance] getSessionCount];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TRSessionCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SessionCell" forIndexPath:indexPath];
    
    TRSession *session = [[TRWhiteboardManager sharedInstance] getSessionAtIndex:indexPath.row];
    
    [cell.titleLabel setText:session.title];
    [cell.sessionImageView setImage:session.image];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[TRWhiteboardManager sharedInstance] prepareWhiteboardFromExistingEnvironmentAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"menuTOcanvas" sender:self];
}



@end
