//
//  TRBackground.h
//  Trio
//
//  Created by J. Christian Bator on 12/16/15.
//  Copyright © 2015 EECS-481. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Firebase/Firebase.h>

@interface TRBackground : NSObject

- (instancetype)initWithSnapshot:(FDataSnapshot *)snapshot;

@property (nonatomic) NSString *filename;

@end
