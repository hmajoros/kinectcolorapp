﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows;

namespace KinectColorApp
{
    public class LayoutController
    {
        public List<Graphic> _graphicList; 
        public Canvas _layoutCanvas;
        public SoundController _soundController;
        private Graphic[] _curGraphics;

        public const int OVER_DRAWING_Z = 3;
        public const int UNDER_DRAWING_Z = 1;
        public const int CODE_DELETE_GRAPHIC = 1;
        public const int CODE_PLAY_SOUND = 2;
        public const int CODE_SHRINK = 3;
        public const int CODE_GROW = 4;
        public const int CODE_TOGGLE_MUSIC = 5;

        public LayoutController(Canvas layoutCanvas, SoundController soundController)
        {
            _graphicList = new List<Graphic>();
            _layoutCanvas = layoutCanvas;
            _soundController = soundController;
        }

        public void ResetAll()
        {
            UpdateAllGraphics(_curGraphics);
        }

        public void UpdateAllGraphics(Graphic[] newGraphics)
        {
            _curGraphics = newGraphics;
            foreach (Graphic g in _graphicList)
            {
                _layoutCanvas.Children.Remove(g.image);
            }

            _graphicList.Clear();

            for (int i = 0; i < newGraphics.Length; ++i) {
                newGraphics[i].image = CreateImage(newGraphics[i], OVER_DRAWING_Z + i);

                _graphicList.Add(newGraphics[i]);
                //Console.WriteLine("adding: " + img.Source);
            }
        }

        public void ReplaceImage(Image victim, Graphic replacement)
        {
            // remove and replace with the other one... description from Graphic object? 
            // make sure replacement is properly centered
            double victimLeft = Canvas.GetLeft(victim);
            double victimTop = Canvas.GetTop(victim);
            double victimHeight = victim.ActualHeight;
            double victimWidth = victim.ActualWidth;
        }

        public Image CreateImage(Graphic graphic, int z)
        {
            Console.WriteLine(graphic.filename);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            // TODO: change this stupid path to resource loader
            string completeUri = @"../../Resources/" + graphic.type + "/" + graphic.category + "/" + graphic.filename;
            bi.UriSource = new Uri(completeUri, UriKind.Relative);
            bi.EndInit();

            TransformedBitmap tb = new TransformedBitmap();
            tb.BeginInit();
            tb.Source = bi;
            var transform = new ScaleTransform(-1, 1);
            tb.Transform = transform;
            tb.EndInit();
            Console.WriteLine("resource uri: " + completeUri);

            //Console.WriteLine("x: " + graphic.GetX() + " y " + graphic.GetY());
            BitmapFrame resizedBitmapFrame = ResizeImage(tb, graphic.GetWidth(), graphic.GetHeight());
            Image img = new Image();
            img.Source = resizedBitmapFrame;

            img.Visibility = Visibility.Visible;
            String name = FilenameToName(graphic.filename);
            Console.WriteLine("img name: " + name);
            img.Name = name;

            _layoutCanvas.Children.Add(img);
            Canvas.SetTop(img, graphic.GetY());
            Canvas.SetLeft(img, graphic.GetX());
            Canvas.SetZIndex(img, z);
            return img;
        }

        private string FilenameToName(string filename)
        {
            string name = filename;
            if (!Char.IsLetter(name[0]))
            {
                name = "_" + name;
            }
            name = name.Substring(0, name.LastIndexOf('.'));
            for(int i = 0; i < name.Length; ++i)
            {
                if (!Char.IsLetterOrDigit(name[i]) && name[i] != '_')
                {
                    Console.WriteLine("replacing: " + name[i] + " with: " + '_');
                    name = name.Replace(name[i], '_');
                }
            }
            return name;
        }

        private BitmapFrame ResizeImage(ImageSource imgSrc, int width, int height)
        {
            Rect rect = new Rect(0, 0, width, height);
            DrawingGroup group = new DrawingGroup();
            RenderOptions.SetBitmapScalingMode(group, BitmapScalingMode.HighQuality);
            group.Children.Add(new ImageDrawing(imgSrc, rect));

            DrawingVisual drawingVisual = new DrawingVisual();
            using (var drawingContext = drawingVisual.RenderOpen())
            {
                drawingContext.DrawDrawing(group);
            }

            var resizedImage = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Default);
            resizedImage.Render(drawingVisual);
            return BitmapFrame.Create(resizedImage);
        }

        public void RemoveLayoutImage(Graphic g)
        {
            _layoutCanvas.Children.Remove(g.image);
            _graphicList.Remove(g);
        }

        public Graphic FindTopMostImageAtPoint(Point coord)
        {
            // grab the topmost image at x, y coordinate
            Graphic[] graphicsAtPoint = FindGraphicsAtPoint(coord);
            int maxZ = -1;
            Graphic maxGraphic = null;
            foreach (Graphic g in graphicsAtPoint)
            {
                int z = Canvas.GetZIndex(g.image);
                Console.WriteLine("z-index: " + z);
                if (z > maxZ && z >= OVER_DRAWING_Z)
                {
                    maxZ = z;
                    maxGraphic = g;
                }
            }
            return maxGraphic;
        }

        public Graphic[] FindGraphicsAtPoint(Point coord)
        {
            double x = coord.X;
            double y = coord.Y;
            var graphics = _graphicList;

            List<Graphic> imgList = new List<Graphic>();
            foreach (var graphic in graphics)
            {
                Image img = graphic.image;
                double left = Canvas.GetLeft(img);
                double top = Canvas.GetTop(img);

                if (x < left || x > left + img.ActualWidth || y < top || y > top + img.ActualHeight)
                {
                    continue;
                }
                else
                {
                    imgList.Add(graphic);
                }
            }
            return imgList.ToArray();
        }

        // does not process dragables, that's a special event
        public void processEvents(Graphic g, int code)
        {
            Console.WriteLine("processing Graphic id = " + g.image.Name + " with code = " + code);
            if (g.events == null) return;

            foreach (Event e in g.events)
            {
                if (e.trigger == code)
                {
                    ExecuteResult(e, g);
                }
            }
        }

        public void MoveGraphic(Graphic g, Point refImageLoc, Point refTouchPoint, Point newLoc)
        {
            double newX = refImageLoc.X + (newLoc.X - refTouchPoint.X);
            double newY = refImageLoc.Y + (newLoc.Y - refTouchPoint.Y);
            Canvas.SetTop(g.image, newY);
            Canvas.SetLeft(g.image, newX);
        }

        public void GrowOrShrink(Image img, bool grow)
        {
            double curHeight = img.ActualHeight;
            double curWidth = img.ActualWidth;
            double changeRatio = 0.2;
            // if shrink, gotta go neg
            if (!grow)
            {
                changeRatio = changeRatio * -1;
            }
            double changeX = curWidth * changeRatio;
            double changeY = curHeight * changeRatio;
            double newWidth = curWidth + changeX;
            double newHeight = curHeight + changeY;
            double origX = Canvas.GetLeft(img);
            double origY = Canvas.GetTop(img);
            double newX = origX - changeX / 2;
            double newY = origY - changeY / 2;
            Canvas.SetLeft(img, newX);
            Canvas.SetTop(img, newY);
            BitmapFrame resizedImage = ResizeImage(img.Source, (int) newWidth, (int) newHeight);
            img.Source = resizedImage;
        }

        public void ExecuteResult(Event e, Graphic g)
        {
            //Console.WriteLine("result: " + e.result + " file: " + e.resource_filename);
            string url;
            switch (e.result)
            {
                case CODE_DELETE_GRAPHIC:
                    RemoveLayoutImage(g);
                    break;
                case CODE_PLAY_SOUND:
                    url = @"../../Resources/" + 
                    e.resource_category + "/" + e.resource_filename;
                    _soundController.PlaySound(url);
                    break;
                case CODE_SHRINK:
                    GrowOrShrink(g.image, false);
                    break;
                case CODE_GROW:
                    GrowOrShrink(g.image, true);
                    break;
                case CODE_TOGGLE_MUSIC:
                    if (SoundController.BackgroundInitialized())
                    {
                        SoundController.ToggleBackground();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
