﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.IO;
using System.Net;
using FireSharp;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Amazon.S3;
using Amazon.S3.Model;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace KinectColorApp
{
    public static class WebController
    {
        private const string _BASE_URL = "http://45.55.153.211/api";
        private const string _CONNECT_URL = "/connect";
        private const string _ENV_URL = "/environment";
        private const int _POLL_RATE_MS = 1000/10;
        private const string _NO_UPDATE_MSG = "No Update";

        private static string _pin;
        private static IFirebaseConfig _config;
        private static IFirebaseClient _client;

        public static event EventHandler<GraphicsData> OnNeedsUI;

        static WebController()
        {
            _config = new FirebaseConfig {
                AuthSecret = "j4b9YFjVUbYY0qS1UjOmdS78qyB5syHf0AVL6QEj",
                BasePath = "https://triotriotrio.firebaseio.com/"
            };
            _client = new FirebaseClient(_config);
        }
        public static string GetPin()
        {
            return _pin;
        }

        public static async void Start()
        {
            Random rnd = new Random();
            _pin = rnd.Next(1000, 9999).ToString();
            //_pin = "9999";

            //FirebaseResponse initialResponse = await _client.GetAsync("environments/" + _pin);
            //if (initialResponse.ResultAs<GraphicsData>() != null)
            //{
            //    Console.WriteLine(initialResponse.Body);
            //    //RaiseOnNeedsUI(initialResponse.ResultAs<GraphicsData>());
            //}

            // send to main thread

            EventStreamResponse StreamResponse = await _client.OnAsync("sessions/" + _pin, changed: (sender, args) =>
            {
                Console.WriteLine("--------update notification recieved-----------");
                Console.WriteLine("version: " + args.Data);
                GetEnvironment();
            });
        }

        public static async void GetEnvironment()
        {
            FirebaseResponse response = await _client.GetAsync("environments/" + _pin);
            Console.WriteLine("--------------environment recieved------------------");
            Console.WriteLine(response.Body);
            GraphicsData newGraphicsDesc = response.ResultAs<GraphicsData>();
            // find any new graphics resources, and grab them from aws
            if (newGraphicsDesc.background_image_filename != null)
            {
                GetImageAWS(newGraphicsDesc.background_image_type, newGraphicsDesc.background_image_category,
                    newGraphicsDesc.background_image_filename);
            }
            if (newGraphicsDesc.background_music_filename != null)
            {
                GetImageAWS(newGraphicsDesc.background_music_type, newGraphicsDesc.background_music_category,
                    newGraphicsDesc.background_music_filename);
            }

            if (newGraphicsDesc.graphics != null)
            {
                foreach (Graphic g in newGraphicsDesc.graphics)
                {
                    Console.WriteLine("graphic: " + g.filename);
                    GetImageAWS(g.type, g.category, g.filename);
                    if (g.events != null)
                    {
                        foreach (Event e in g.events)
                        {
                            if (e.resource_filename != null)
                            {
                                GetImageAWS(e.resource_type, e.resource_category, e.resource_filename);
                            }
                        }
                    }
                }
            }
            RaiseOnNeedsUI(newGraphicsDesc);
        }

        private static void RaiseOnNeedsUI(GraphicsData data)
        {
            if (OnNeedsUI != null)
            {
                OnNeedsUI(null, data);
            }
        }

        public static void GetImageAWS(string type, string category, string filename)
        {
            Console.WriteLine("getimageaws");
            string destFolderPath;
            string surl;
            if (type == null)
            {
                destFolderPath = @"../../Resources/" + category;
                surl = "https://s3.amazonaws.com/triotriotrio/" + category +
                "/" + filename;
            }
            else
            {
                destFolderPath = @"../../Resources/" + type + "/" + category;
                surl = "https://s3.amazonaws.com/triotriotrio/" + type + "/" + category +
                "/" + filename;
            }
            string destPath = destFolderPath + "/" + filename;
            DirectoryInfo di = Directory.CreateDirectory(destFolderPath);
            Uri suri = new Uri(surl);

            using (WebClient webClient = new WebClient())
            {
                while (!File.Exists(destPath))
                {
                    Console.WriteLine("Downloading file: " + destPath);
                    webClient.DownloadFile(suri, destPath);
                }
                Console.WriteLine("file found: " + destPath);
            }
        }
    }

    public class GraphicsData
    {
        public GraphicsData() { }
        public string background_color { get; set; }
        public string background_image_filename { get; set; }
        public string background_image_category { get; set; }
        public string background_image_type { get; set; }
        public string background_music_type { get; set; }
        public string background_music_category { get; set; }
        public string background_music_filename { get; set; }
        public List<Graphic> graphics { get; set; }
        // arbitrary events handled by trigger/response pairs
        public List<Event> events{ get; set; }
    }

    public class Event
    {
        public Event() { }
        /* types of triggers:
            - firstTouch = 1
            - longPress = 2
            - draggable = 3
        */
        public int trigger;

        /* types of results
            - deleteGraphic = 1
            - playSound = 2
            - shrink = 3
            - grow = 4
            - toggle sound = 5
        */
        public int result;
        public string resource_type;
        public string resource_category;
        public string resource_filename;
        public GraphicAnimation animationInfo;
    }

    public class Graphic
    {
        public const double CANVAS_HEIGHT = 600;
        public const double CANVAS_WIDTH = 800;

        public Graphic() { }
        public Graphic(string id, string filename, string type, string category, double x_ratio, double y_ratio, 
            double width_ratio, double height_ratio)
        {
            this.id = id;
            this.filename = filename;
            this.type = type;
            this.category = category;
            this.x_ratio = x_ratio;
            this.y_ratio = y_ratio;
            this.width_ratio = width_ratio;
            this.height_ratio = height_ratio;
        }
        public string id { get; set; }
        public string filename { get; set; }
        public double x_ratio { get; set; }
        public double y_ratio { get; set; }
        public double width_ratio { get; set; }
        public double  height_ratio { get; set; }
        public string type { get; set; }
        public string category { get; set; }
        public List<Event> events { get; set; }
        public Image image { get; set; }

        public int GetWidth()
        {
            return (int) (width_ratio * CANVAS_WIDTH);
        }

        public int GetHeight()
        {
            return (int) (height_ratio * CANVAS_HEIGHT);
        }

        public double GetX()
        {
            return (x_ratio * CANVAS_WIDTH);
        }

        public double GetY()
        {
            return (y_ratio * CANVAS_HEIGHT);
        }
    }

    public class GraphicAnimation
    {
        //public Coord start;
        //public Coord end;
        //public int duration;
    }
}