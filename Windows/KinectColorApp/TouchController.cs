using System;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Controls;
using System.Timers;
using System.Windows;

namespace KinectColorApp
{
    public class TouchController
    {
        const int FirstTouchCode = 1;
        const int LongPressCode = 2;

        LayoutController layoutController;
        SoundController soundController;
        const int longPressLength = 1500;
        const int timeoutInterval = 500;
        const int continuousPressInterval = 100;
        const int CODE_DRAGGABLE = 3;

        private bool longPressActivated = false;
        private bool touchActive;
        private TouchEvent lastTouch;
        private DateTime longPressStart;

        private Timer touchTimeoutTimer;
        private Point initialTouchPoint;
        private Point initialImageLoc;
        private Point prevMovePoint;
        private const int MOVE_THRESHOLD = 4;


        public TouchController(LayoutController layoutController, SoundController soundController)
        {
            this.soundController = soundController;
            this.layoutController = layoutController;
            touchActive = false;
            lastTouch = null;

            touchTimeoutTimer = new Timer();
            touchTimeoutTimer.Interval = timeoutInterval;
            touchTimeoutTimer.Elapsed += ResetLongPressTimer;
        }

        public void TouchReceived(TouchEvent touch)
        {
            touchTimeoutTimer.Stop();
            touchTimeoutTimer.Start();

            if (touch.graphic != null)
            {
                if (!touchActive)
                {
                    Console.WriteLine("---------First touch----------");
                    longPressStart = touch.timestamp;
                    touchActive = true;
                    layoutController.processEvents(touch.graphic, FirstTouchCode);
                    // grab initial reference point
                    initialTouchPoint = touch.touchPoint;
                    double imgLocX = Canvas.GetLeft(touch.graphic.image); 
                    double imgLocY = Canvas.GetTop(touch.graphic.image);

                    initialImageLoc = new Point(imgLocX, imgLocY);
                }
                else
                {
                    if (lastTouch == null || Canvas.GetZIndex(touch.graphic.image) != Canvas.GetZIndex(lastTouch.graphic.image))
                    {
                        Console.WriteLine("Restting Long Press!");
                        longPressStart = touch.timestamp;
                    } 
                    TimeSpan span = touch.timestamp - longPressStart;
                    Console.WriteLine(span.TotalMilliseconds);
                    if ((int)span.TotalMilliseconds > longPressLength && !longPressActivated)
                    {
                        Console.WriteLine("(touch): Registered Long Press!");
                        layoutController.processEvents(touch.graphic, LongPressCode);
                        longPressActivated = true;
                    }

                    // check for draggable
                    if ((int) span.TotalMilliseconds > timeoutInterval)
                    {
                        Console.WriteLine("Move Graphic: ");
                        //if they haven't moved on, we can do click & drag here
                        if (HasMoved(touch.touchPoint))
                        {
                            if (touch.graphic.events != null)
                            {
                                foreach (Event e in touch.graphic.events)
                                {
                                    if (e.trigger == CODE_DRAGGABLE)
                                    {
                                        layoutController.MoveGraphic(touch.graphic, initialImageLoc, initialTouchPoint, touch.touchPoint);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                lastTouch = touch;
            }
            else
            {
                lastTouch = null;
            }
        }

        private bool HasMoved(Point p)
        {
            if (p.X < prevMovePoint.X - MOVE_THRESHOLD ||
                p.Y < prevMovePoint.Y - MOVE_THRESHOLD ||
                p.X > prevMovePoint.X + MOVE_THRESHOLD ||
                p.Y > prevMovePoint.Y + MOVE_THRESHOLD)
            {
                prevMovePoint = p;
                return true;
            } else
            {
                return false;
            }
        }

        private void ResetLongPressTimer(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("RESETTING TOUCH!");
            touchTimeoutTimer.Stop();
            touchActive = false;
            longPressActivated = false;
        }
    }

    /* TouchEvent Class which assists with creating and recieving 
     * touch events. each event has coordinates, and information about
     * graphics the touch event may encounter
     */
    public class TouchEvent
    {
        public Point touchPoint;
        public Graphic graphic;
        public DateTime timestamp;

        public TouchEvent(Point coord, Graphic graphic)
        {
            this.touchPoint = coord;
            this.graphic = graphic;
            timestamp = DateTime.Now;
        }
    }
}

