# Trio

EECS 481 - University of Michigan - Fall 2015

Christian Bator, Sam Lu, Henry Majoros, and Jesse Stuart

# Background:
Trio is the bond between patient, caregiver, and technology. It’s a workflow for caregivers that allows them to manage all of their patients’ specific needs. Trio is not a single app-based therapy, but it is rather a simple way for caregivers to tailor three-dimensional therapy to each patient in his or her practice. Autism does not have a one-size-fits-all treatment, so the technology must be dynamic. With Trio, caregivers can manage the profile of each patient, create custom therapies to address each patient’s needs, and record the results of the session. Trio places the control in the hands of those who know the patients the best, caregivers.

# Getting started:

Windows


iOS

